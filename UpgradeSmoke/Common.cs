﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace UpgradeSmoke
{
        [TestFixture]
        public class Common
        {
        #region Driver
        public IWebDriver driver;
        public string testMethodName;
        public bool value = false;
        public string Browserstack_userName = "banupriya6";
        public string Browserstack_Key = "yxk6kC8WU46MzchoMLh2";

        //Browser values = Chrome, Firefox, IE, Iphone, Android
        //env values = Upgrade_Dev, Upgrade_Test, Upgarde_QA, QA_Akamai
        public string Browser = "Chrome";
        public string env = "Upgarde_QA";

        public void Start(string navurl)
        {
            string Navigationurl = navurl;
            DesiredCapabilities capability = new DesiredCapabilities();
            capability.SetCapability("browserstack.user", Browserstack_userName);
            capability.SetCapability("browserstack.key", Browserstack_Key);
            capability.SetCapability("browserstack.local", "false");
            capability.SetCapability("browserstack.debug", "true");
            capability.SetCapability("browserstack.networkLogs", "true");
            capability.SetCapability("Project", "UpgardeSmoke");
            capability.SetCapability("build", "UpgradeSmoke081217");
            capability.SetCapability("name", testMethodName);

            switch (Browser)
            {
                case "Chrome":
                    capability.SetCapability("browser", "Chrome");
                    capability.SetCapability("browser_version", "61.0");
                    capability.SetCapability("os", "Windows");
                    capability.SetCapability("os_version", "10");
                    capability.SetCapability("resolution", "1280x800");
                    capability.SetCapability("quality", "Compressed");
                    break;

                case "Firefox":
                    capability.SetCapability("browser", "Chrome");
                    capability.SetCapability("browser_version", "61.0");
                    capability.SetCapability("os", "Windows");
                    capability.SetCapability("os_version", "10");
                    break;

                case "IE":
                    capability.SetCapability("browser", "IE");
                    capability.SetCapability("browser_version", "11.0");
                    capability.SetCapability("os", "Windows");
                    capability.SetCapability("os_version", "10");
                    capability.SetCapability("resolution", "1024x768");
                    break;

                case "Iphone":
                    capability.SetCapability("device", "iPhone 7");
                    capability.SetCapability("realMobile", "true");
                    capability.SetCapability("os_version", "10.0");
                    break;

                case "Android":
                    capability.SetCapability("device", "Samsung Galaxy S8 Plus");
                    capability.SetCapability("realMobile", "true");
                    capability.SetCapability("os_version", "7.0");
                    break;

                default:
                    Console.WriteLine("Please select browser");
                    break;
            }

            driver = new RemoteWebDriver(new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability);
            driver.Manage().Window.Maximize();

            switch (env)
            {
                case "Upgrade_Test":
                    driver.Navigate().GoToUrl("https://vm-netstxp-cd.northeurope.cloudapp.azure.com/" + Navigationurl);
                    break;

                case "Upgrade_Dev":
                    driver.Navigate().GoToUrl("https://vm-netstupg-cd.northeurope.cloudapp.azure.com/" + Navigationurl);
                    break;

                case "Upgarde_QA":
                    driver.Navigate().GoToUrl("https://neqaoxp-cd.northeurope.cloudapp.azure.com/" + Navigationurl);
                    break;

                case "QA_Akamai":
                    string test1 = @"https://" + "volvo-web" + ":" + "v0lv0!%40%23" + "@" + "qaassets.volvocars.com";
                    driver.Navigate().GoToUrl(test1);
                    string test2 = @"https://" + "volvo-web" + ":" + "v0lv0!%40%23" + "@" + "qaoxp.volvocars.com/";
                    driver.Navigate().GoToUrl(test2);
                    string test = @"https://" + "volvo-web" + ":" + "v0lv0!%40%23" + "@" + "qaoxp.volvocars.com/" + Navigationurl;
                    driver.Navigate().GoToUrl(test);
                    break;
            }
        }


        public void BrowserstackPass()
        {

            string reqString = "{\"status\":\"passed\", \"reason\":\"Test Scenario passed\"}";

            byte[] requestData = Encoding.UTF8.GetBytes(reqString);
            Uri myUri = new Uri(string.Format("https://www.browserstack.com/automate/sessions/" + getSessionID() + ".json"));
            WebRequest myWebRequest = HttpWebRequest.Create(myUri);
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)myWebRequest;
            myWebRequest.ContentType = "application/json";
            myWebRequest.Method = "PUT";
            myWebRequest.ContentLength = requestData.Length;
            using (Stream st = myWebRequest.GetRequestStream()) st.Write(requestData, 0, requestData.Length);

            NetworkCredential myNetworkCredential = new NetworkCredential(Browserstack_userName, Browserstack_Key);
            CredentialCache myCredentialCache = new CredentialCache();
            myCredentialCache.Add(myUri, "Basic", myNetworkCredential);
            myHttpWebRequest.PreAuthenticate = true;
            myHttpWebRequest.Credentials = myCredentialCache;

            myWebRequest.GetResponse().Close();
        }

        public void BrowserstackFail()
        {

            string reqString = "{\"status\":\"error\", \"reason\":\"Test Scenario Failed\"}";

            byte[] requestData = Encoding.UTF8.GetBytes(reqString);
            Uri myUri = new Uri(string.Format("https://www.browserstack.com/automate/sessions/" + getSessionID() + ".json"));
            WebRequest myWebRequest = HttpWebRequest.Create(myUri);
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)myWebRequest;
            myWebRequest.ContentType = "application/json";
            myWebRequest.Method = "PUT";
            myWebRequest.ContentLength = requestData.Length;
            using (Stream st = myWebRequest.GetRequestStream()) st.Write(requestData, 0, requestData.Length);

            NetworkCredential myNetworkCredential = new NetworkCredential(Browserstack_userName, Browserstack_Key);
            CredentialCache myCredentialCache = new CredentialCache();
            myCredentialCache.Add(myUri, "Basic", myNetworkCredential);
            myHttpWebRequest.PreAuthenticate = true;
            myHttpWebRequest.Credentials = myCredentialCache;

            myWebRequest.GetResponse().Close();
        }

        [TearDown]
        public void Quit()
        {
            driver.Quit();
        }
        #endregion
        #region Common methods
        public string GetCallerMethodName()
        {
            var stackTrace = new StackTrace();
            StackFrame stackFrame = stackTrace.GetFrame(1);
            MethodBase methodBase = stackFrame.GetMethod();
            //MethodBase currentmethod = MethodBase.GetCurrentMethod();
            return methodBase.Name;
        }

        public string getSessionID()
        {
            String sessionId = ((RemoteWebDriver)driver).SessionId.ToString();
            return sessionId;
        }

        public void RefreshPage()
        {
            driver.Navigate().Refresh();
        }
        public void waitfor(int timeunit)
        {
            Thread.Sleep(timeunit);
        }

        public IWebElement FindElement(string xpath)
        {
            IWebElement elementToFind = driver.FindElement(By.XPath(xpath));
            return elementToFind;
        }

        public bool VerifyImagePresent(string xpath)
        {
            try
            {
                string value = driver.FindElement(By.XPath(xpath)).GetAttribute("src");
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public bool IsElementDisplayed(string xpath)
        {
            try
            {
                bool value = driver.FindElement(By.XPath(xpath)).Displayed;
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        public string GetattributeValue(string xpath, string attrname)
        {
            string attrval = driver.FindElement(By.XPath(xpath)).GetAttribute(attrname);
            return attrval;
        }
        public void verifyattributepresent(string xpath, string attrname)
        {
            string attrval = driver.FindElement(By.XPath(xpath)).GetAttribute(attrname);
            Console.WriteLine(attrval);
            Assert.IsNotNull(attrval);
        }
        public void verifyIfDisplayed(String xpath)
        {
            bool val = driver.FindElement(By.XPath(xpath)).Displayed;
            Assert.IsTrue(val);
            Console.WriteLine("Element is displayed successfully");
        }

        public void Verifytext(String xpath, String text)
        {
            String actualtext = driver.FindElement(By.XPath(xpath)).Text;
            Assert.AreEqual(text, actualtext);
            Console.WriteLine(actualtext + " is displayed successfully");
        }

        public void Mouseoverelement(string xpath)
        {
            IWebElement element = driver.FindElement(By.XPath(xpath));
            Actions act = new Actions(driver);
            act.MoveToElement(element).Build().Perform();
        }
        public bool IsElementActive(string xpath)
        {
            try
            {
                bool value = driver.FindElement(By.XPath(xpath)).Selected;
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public string SelectText(string xpath)
        {
            string value = driver.FindElement(By.XPath(xpath)).Text;
            return value;
        }

        public void Click_Element(string xpath)
        {
            IWebElement elementToClick = driver.FindElement(By.XPath(xpath));
            elementToClick.Click();
        }

        public void Menu_Click(string xpath)
        {
            IWebElement elementToClick = driver.FindElement(By.XPath(xpath));
            Actions act = new Actions(driver);
            act.MoveToElement(elementToClick).Click().Build().Perform();
            Thread.Sleep(15000);
        }

        //To Enter text 
        public void EnterText(string xpath, string TextInput)
        {
            IWebElement Element = driver.FindElement(By.XPath(xpath));
            Element.Clear();
            Element.SendKeys(TextInput);
        }

        //To get value from text box
        public string GetValue(string xpath)
        {
            string value = driver.FindElement(By.XPath(xpath)).GetAttribute("value");
            return value;
        }

        //To wait and check if an element is displayed(for Dynamic elements)
        public void FindDynamicElementandValidate(string xpath)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            bool value = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(xpath))).Displayed;
            Assert.AreEqual(true, value);
        }

        //Select dropdownvalue
        public void SelectDropdownValue(string xpath, string value)
        {
            IWebElement ElementToSelect = FindElement(xpath);
            var SelectElement = new SelectElement(ElementToSelect);
            SelectElement.SelectByValue(value);
        }

        //Scroll into view
        public void ScrollIntoView(string xpath)
        {
            IWebElement element = driver.FindElement(By.XPath(xpath));
            Actions action = new Actions(driver);
            action.MoveToElement(element);
            action.Perform();
            System.Threading.Thread.Sleep(2000);
        }
        //Select link 
        public void SelectAnchorListValue(string value)
        {
            //List <IWebElement> list = new List <IWebElement>();
            driver.FindElement(By.LinkText(value)).Click();
            System.Threading.Thread.Sleep(2000);
        }

        public string GenerateNumber()
        {
            Random random = new Random();
            long Number = random.Next(777777777, 999999999);
            string ElementToReturn = Number.ToString();
            return ElementToReturn;
        }

        public string PageTitle()
        {
            string PageTitle = driver.Title;
            return PageTitle;
        }

        public string PageUrl()
        {
            string PageUrl = driver.Url;
            return PageUrl;
        }
        #endregion
    }
}
