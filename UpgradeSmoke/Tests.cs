﻿using System;
using NUnit.Framework;

namespace UpgradeSmoke
{
    [TestFixture]
    public class Tests : Methods
    {
        public string navurl;
        public bool setvalue = false;
        #region StoryTelling
        [Test]
        [Category("StoryTelling")]
        public void StoryTelling_INTL()
        {
            try
            {
                testMethodName = GetCallerMethodName();
                navurl = "intl";
                Start(navurl);
                StoryTellingIntl();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }

        [Test]
        [Category("StoryTelling")]
        public void StoryTelling_UK()
        {
            try
            {
                testMethodName = GetCallerMethodName();
                navurl = "uk";
                Start(navurl);
                StoryTellingUK();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }
        #endregion

        #region ChildSeat
        [Test]
        [Category("DcomChildSeat")]
        public void DCOMChildSeat_INTL()
        {
            try
            {

                testMethodName = GetCallerMethodName();
                navurl = "intl/buy/purchase/child-seats";
                Start(navurl);
                ChildSeatINTL();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }

        [Test]
        [Category("DcomChildSeat")]
        public void DCOMChildSeat_Sweden()
        {
            try
            {
                testMethodName = GetCallerMethodName();
                navurl = "se/aga/tillbehor/bilbarnstolar";
                Start(navurl);
                ChildSeatUK();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }
        #endregion

        #region LXP
        [Test]
        [Category("LXP")]
        public void LXP_SignIn()
        {
            try
            {
                testMethodName = GetCallerMethodName();
                navurl = "uk";
                Start(navurl);
                SignInUK();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }

        [Test]
        [Category("LXP")]
        public void LXP_MyCars()
        {
            try
            {
                testMethodName = GetCallerMethodName();
                navurl = "uk";
                Start(navurl);
                SignInUK();
                AddCarsUK();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }

        [Test]
        [Category("LXP")]
        public void LXP_PreferredDealer()
        {
            try
            {
                testMethodName = GetCallerMethodName();
                navurl = "uk";
                Start(navurl);
                SignInUK();
                ValidatePreferredDealer();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }

        [Test]
        [Category("LXP")]
        public void LXP_ProfileSettings()
        {
            try
            {
                testMethodName = GetCallerMethodName();
                navurl = "uk";
                Start(navurl);
                SignInUK();
                validateprofilesetting();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }
        #endregion

        #region CarLocator
        [Test]
        [Category("CarLocator")]
        public void CarLocator_SearchResult()
        {
            try
            {
                testMethodName = GetCallerMethodName();
                navurl = "us";
                Start(navurl);
                Inventory_Validation();
                InvetorySearchResultsValidation();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }

        [Test]
        [Category("CarLocator")]
        public void CarLocator_ProductDetailPage()
        {
            try
            {
                testMethodName = GetCallerMethodName();
                navurl = "us";
                Start(navurl);
                Inventory_Validation();
                ValidateProductDetailPage();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }
        #endregion

        #region DcomItaly
        [Test]
        [Category("Dcom Volvo Studio")]
        public void DcomItaly()
        {
            try
            {
                testMethodName = GetCallerMethodName();
                navurl = "it/mondo-volvo/iniziative/studio-milano";
                Start(navurl);
                OrderOnlineNavigation();
                CarRentalsPageValidation_XC60();
                RentalFormPageValidation_XC60();
                SubmitOrderPageValidation_XC60();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }
        #endregion

        #region DcomAccessories
        [Test]
        [Category("Dcom Accessories")]
        public void DcomAccessories_UK()
        {
            testMethodName = GetCallerMethodName();
            navurl = "uk/accessories";
            Start(navurl);
            uk_validateLandingpage();
            uk_validateNavigationbar();
            uk_DealerLocator();
            uk_CarSelector();
            uk_validateAutoAccessoriesPage();
            uk_validateLifestylecollectionPage();
            uk_validatesearchresultpage();
            uk_validatproductdetailspage();
            uk_checkoutpage();
        }
        #endregion

        #region DcomRegularSales
        [Test]
        [Category("Dcom Regular Sales")]
        public void DcomRegularSales_DealerAndBuildValidations()
        {
            try
            {
                testMethodName = GetCallerMethodName();
                navurl = "no/buy/sales/care-by-volvo";
                Start(navurl);
                CareByVolvoHomePageValidation();
                DealerSelectionValidation();
                BuildPageValidation();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }

        [Test]
        [Category("Dcon Regular Sales")]
        public void DcomReularSales_FinancialOptions()
        {
            try
            {
                testMethodName = GetCallerMethodName();
                navurl = "no/buy/sales/care-by-volvo/financialoptions?CarProductXmlId=8fda12d1-455f-4ef0-86ad-b99c27fa483b";
                Start(navurl);
                FinancialoptionsValidation();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }
        #endregion

        #region DcomGeneralSales
        [Test]
        [Category("Dcom General Sales")]
        public void DcomGeneralSales_S90()
        {
            try
            {
                testMethodName = GetCallerMethodName();
                navurl = "us/order-yours/orderpreview?carProductXmlID=54bc5ec7-22a5-4574-a130-2041d4b08fd4&uniqueConfigurationId=e7945bb0-d5ae-45c3-98e1-e8b0f71256d0";
                //navurl = "us/order-yours/orderpreview?carProductXmlID=cf646770-3474-4964-bd9f-d22153029696&uniqueConfigurationId=3998c305-db95-43da-bf9a-a8d12b6ef53a";
                Start(navurl);
                OrderPreviewPageS90();
                OrderSummaryPageS90();
                PaymentPage();
            }
            catch
            {
                setvalue = true;
            }
            finally
            {
                if (setvalue == true) { BrowserstackFail(); }
                else { BrowserstackPass(); }
            }
        }
        #endregion
    }
}
