﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpgradeSmoke
{
    public class ElementRepository: Common
    {
        #region Story Telling
        //TS1
        public string Discover_Volvo_Menu = "//*[@id='v2-mob']/div[2]/div[2]/a";
        public string IntlStoryTelling_Pageurl = "https://vm-netstxp-cd.northeurope.cloudapp.azure.com/intl/discover-volvo";
        public string Most_Recent_tab = "//*[@id='storytelling-subnav']/div[2]/a[1]"; 
        public string Most_Popular_tab = "//*[@id='storytelling-subnav']/div[2]/a[2]";
        public string ByTopic_Menu = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/p";
        public string ByCar_Menu = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/p";

        public string Car_Accessories_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[1]/div[1]/a";
        public string Human_Made_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[1]/div[2]/a";
        public string Ownership_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[1]/div[3]/a";
        public string Services_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[1]/div[4]/a";
        public string Collaboration_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[5]/div[1]/a";
        public string XC70_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[5]/div[2]/a";
        public string S60CC_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[5]/div[3]/a";
        public string Business_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[2]/div[1]/a";
        public string Working_At_Volvo_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[2]/div[2]/a";
        public string Heritage_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[2]/div[3]/a";
        public string City_Life_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[2]/div[4]/a";
        public string Design_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[3]/div[1]/a";
        public string Environment_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[3]/div[2]/a";
        public string Lifestyle_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[3]/div[3]/a";
        public string Future_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[3]/div[4]/a";
        public string Family_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[4]/div[1]/a";
        public string Safety_Performance_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[4]/div[2]/a";
        public string Technology_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[4]/div[3]/a";
        public string Events_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[4]/div[4]/a";

        public string XC90_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[1]/div[1]/a";
        public string XC60_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[1]/div[2]/a";
        public string XC40_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[1]/div[3]/a";
        public string V90_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[1]/div[4]/a";
        public string V60_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[2]/div[1]/a";
        public string V40_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[2]/div[2]/a";
        public string S90_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[2]/div[3]/a";
        public string V60CC_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[2]/div[4]/a";
        public string V40CC_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[3]/div[1]/a";
        public string V90CC_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[3]/div[2]/a";
        public string SUV_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[3]/div[3]/a";
        public string Sedan_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[3]/div[4]/a";
        public string Estate_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[4]/div[1]/a";
        public string CrossCountry_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[4]/div[2]/a";
        public string ConceptCars_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[4]/div[3]/a";

        public string MainStories_Module = "//*[@id='volvo']/div[3]/div[1]"; 
        public string TextInfo_Module = "//*[@id='volvo']/div[3]/div[2]";
        public string OtherStories_Module = "//*[@id='volvo']/div[3]/div[3]";

        //TS2
        public string Discover_Menu = "//*[@id='DiscoverVolvo']";
        public string Discover_Volvo = "//*[@id='all_Discover_Volvo']/div[1]/a[1]/div/div/div[2]"; 
        public string UKStoryTelling_Pageurl = "https://vm-netstxp-cd.northeurope.cloudapp.azure.com/uk/about/humanmade/discover-volvo";

        public string UK_Heritage_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[2]/div[2]/a";
        public string UK_CityLife_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[2]/div[3]/a";
        public string UK_Design_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[2]/div[4]/a";
        public string UK_Environment_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[3]/div[1]/a";
        public string UK_Lifestyle_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[3]/div[2]/a";
        public string UK_Future_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[3]/div[3]/a";
        public string UK_Family_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[3]/div[4]/a";
        public string UK_Safety_Performance_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[4]/div[1]/a";
        public string UK_Technology_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[4]/div[2]/a";
        public string UK_Events_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[4]/div[3]/a";
        public string UK_Collaboration_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[1]/div/div/div[4]/div[4]/a";

        public string UK_S60_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[2]/div[4]/a";
        public string UK_V90CC_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[3]/div[1]/a";
        public string UK_V60CC_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[3]/div[2]/a";
        public string UK_V40CC_Link = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[3]/div[3]/a";
        public string SUV = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[3]/div[4]/a";
        public string Sedan = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[4]/div[2]/a";
        public string CrossCountry = "//*[@id='storytelling-subnav']/div[3]/div[1]/div/div[2]/div/div/div[4]/div[3]/a";
        #endregion

        #region Childseat
        //TS3
        public const string intl_babySeat = "//*[@id='volvo']/div[6]/ul/li[1]/img";
        public const string intl_back2childSeat = "//*[@id='volvo']/div[6]/ul/li[2]/img";
        public const string intl_boosterSeat = "//*[@id='volvo']/div[6]/ul/li[3]/img";
        public const string intl_boosterSeatcushion = "//*[@id='volvo']/div[6]/ul/li[4]/img";
        public const string infantseatImage = ".//*[@id='child-seat-infant-seat']/div/div[1]/div/img";
        public const string infantseatHeader = ".//*[@id='child-seat-infant-seat']/div/div[2]/h2";
        public const string rearwardFacingseatImage = ".//*[@id='child-seat-rearward-facing']/div/div[2]/div/img";
        public const string rearwardFacingseatHeader = ".//*[@id='child-seat-rearward-facing']/div/div/h2";
        public const string boosterSeatImage = ".//*[@id='child-seat-booster-seat']/div/div[1]/div/img";
        public const string boosterSeatHeader = ".//*[@id='child-seat-booster-seat']/div/div[2]/h2";
        public const string boosterSeatCushionImage = ".//*[@id='child-seat-booster-cushion']/div/div[2]/div/img";
        public const string boosterSeatCushionHeader = ".//*[@id='child-seat-booster-cushion']/div/div/h2";
        public const string intl_readmoreInfantSeat = "//*[@id='volvo']/div[6]/ul/li[1]/div/a";

        //TS4
        public const string se_infantSeat = "//*[@id='volvo']/div[6]/ul/li[1]/img";
        public const string se_childSeat = "//*[@id='volvo']/div[6]/ul/li[2]/img";
        public const string se_highchairbooster = "//*[@id='volvo']/div[6]/ul/li[3]/img";
        public const string se_boosterSeatbackrest = "//*[@id='volvo']/div[6]/ul/li[4]/img";
        public const string readmoreboosterSeatbackrest = "//*[@id='volvo']/div[6]/ul/li[4]/div/a";
        #endregion

        #region LXP
        //TS1
        public const string icon_lxplogin = "//*[@id='v2-mob']/div[2]/div[5]/div[2]";
        public const string link_signin = "//*[@id='v2-mob']/div[2]/nav/a[1]";
        public const string link_createvolvoid = "//*[@id='v2-mob']/div[2]/nav/a[2]";
        public const string text_username = "//*[@id='loginField']";
        public const string text_password = "//*[@id='pwdField']";
        public const string text_username_val = "testlxp2@mailinator.com";
        public const string text_password_val = "Passw0rd";
        public const string btn_login = "//*[@id='loginButton']";
        public const string link_forgotpassword = "//*[@id='forgotPassword']";
        public const string link_whatisvolvoid = "//*[@id='aboutVolvoId']";
        public const string link_getvolvoid = "//*[@id='getVolvoId']";
        public const string txt_greeting = "//*[@id='v2-mob']/div[2]/nav/sapn";
        public const string LXPMenu_mycars = "//*[@id='v2-mob']/div[2]/nav/a[1]";
        public const string LXPMenu_mycars_val = "My Cars";
        public const string LXPMenu_preferredretailer = "//*[@id='v2-mob']/div[2]/nav/a[2]";
        public const string LXPMenu_preferredretailer_val = "Preferred Retailer";
        public const string LXPMenu_Newstories = "//*[@id='v2-mob']/div[2]/nav/a[3]";
        public const string LXPMenu_Newstories_val = "New Stories";
        public const string LXPMenu_Offers = "//*[@id='v2-mob']/div[2]/nav/a[4]";
        public const string LXPMenu_Offers_val = "Offers";
        public const string LXPMenu_profilesetting = "//*[@id='v2-mob']/div[2]/nav/a[5]";
        public const string LXPMenu_profilesetting_val = "Profile and Settings";
        public const string LXPMenu_signout = "//*[@id='v2-mob']/div[2]/nav/a[6]";
        public const string LXPMenu_signout_val = "Sign Out";
        //TS2
        public const string txt_hdr_mycars = "//*[@id='volvo']/div[3]/h2";
        public const string txt_hdr_mycars_val = "My Cars";
        public const string txtbx_vincarinput = "//*[@id='volvo']/div[3]/section/div/div[1]/input";
        public const string txtbx_vincarinput_val = "YV1MZ845BE2034586";
        public const string btn_addcar = "//*[@id='volvo']/div[3]/section/div/div[2]/a";
        public const string addcar_sampleimg = "//*[@id='lxp-mask']/div/div[3]/div/div/div[1]/img";
        public const string addcar_modelname = "//*[@id='lxp-mask']/div/div[3]/div/div/div[2]/ul[1]/li[1]/span[2]";
        public const string addcar_productionyr = "//*[@id='lxp-mask']/div/div[3]/div/div/div[2]/ul[2]/li[1]/span[2]";
        public const string addcar_carrelation = "//*[@id='lxp-mask']/div/div[3]/div/div/div[2]/ul[1]/li[2]/span[2]";
        public const string addcar_vinnum = "//*[@id='lxp-mask']/div/div[3]/div/div/div[2]/ul[2]/li[2]/span[2]";
        public const string addcar_cancelbtn = "//*[@id='lxp-mask']/div/div[4]/div[1]/a[2]";
        public const string addcar_confirmbtn = "//*[@id='lxp-mask']/div/div[4]/div[1]/a[1]";
        public const string addedcar_img = "//*[@id='volvo']/div[3]/div[1]/ul/li[2]/div[1]/div[1]/img";
        public const string addedcar_modelname = "//*[@id='volvo']/div[3]/div[1]/ul/li[2]/div/div[2]/div[1]/ul[1]/li[1]/span[2]";
        public const string addedcar_modelyr = "//*[@id='volvo']/div[3]/div[1]/ul/li[2]/div/div[2]/div[1]/ul[2]/li[1]/span[2]";
        public const string addedcar_color = "//*[@id='volvo']/div[3]/div[1]/ul/li[2]/div/div[2]/div[1]/ul[1]/li[2]/span[2]";
        public const string addedcar_relation = "//*[@id='volvo']/div[3]/div[1]/ul/li[2]/div/div[2]/div[1]/ul[2]/li[2]/span[2]";
        public const string addedcar_vin = "//*[@id='volvo']/div[3]/div[1]/ul/li[2]/div/div[2]/div[1]/ul[1]/li[3]/span[2]";
        public const string addedcar_Licenseplate = "//*[@id='volvo']/div[3]/div[1]/ul/li[2]/div/div[2]/div[1]/ul[2]/li[3]/div";
        public const string addcar_setasmaincarlink = "//*[@id='volvo']/div[3]/div[1]/ul/li[2]/div/div[2]/div[2]/div[2]/a";
        public const string addcar_changecarreltnlink = "//*[@id='volvo']/div[3]/div[1]/ul/li[2]/div/div[2]/div[2]/div[3]/a";
        public const string changecar_reqownerradio = "//*[@id='lxp-mask']/div/div[2]/div/div/div[2]/div[1]/fieldset/label";
        public const string changecar_removeradio = "//*[@id='lxp-mask']/div/div[2]/div/div/div[2]/div[2]/fieldset/label";
        public const string changecar_cancelbtn = "//*[@id='lxp-mask']/div/div[3]/div/a[2]";
        public const string changecar_confirmbtn = "//*[@id='lxp-mask']/div/div[3]/div/a[1]";
        public const string addedcar_maincar = "//*[@id='volvo']/div[3]/div[1]/ul/li[2]/div/div[2]/div[2]/div[2]/span";
        public const string addedcar_maincartxt_val = "Main Car";
        public const string addedcar2_setasmaincarlink = "//*[@id='volvo']/div[3]/div[1]/ul/li[2]/div[2]/a[1]";
        public const string addedcar_requestownershipsuccessmsg = "//*[@id='volvo']/div[3]/div[1]/ul/li[1]/div[1]";
        public const string addedcar_requestownershipsuccessmsg_val = "Thank you! The requested ownership will be reviewed and updated within a few days.";
        public const string addedcar_removedsuccessmsg = "//*[@id='volvo']/div[3]/div[1]/div[3]";
        public const string addedcar_removedsuccessmsgval = "Thank you! The car has been disconnected.";
        public const string addcar2_changecarreltnlink = "//*[@id='volvo']/div[3]/div[1]/ul/li[2]/div[2]/a";
        public const string validvinrequest = "//*[@id='volvo']/div[3]/section/div/div[1]/div[2]";
        public const string validvinrequest_val = "Please enter a valid VIN number.";
        public const string addsuccessmsg = "//*[@id='volvo']/div[3]/div[1]/div[3]";
        public const string addsuccessmsgval = "Your car has been added. Thank you!";
        //TS3
        public const string prefdlr_changelink = "//*[@id='volvo']/div[3]/form/section/div/div/div[1]/p/a";
        public const string prefdlr_srchtxtbx = "//*[@id='dealer-picker-query']";
        public const string prefdlr_zipcode = "98901";
        public const string prefdlr_searchicon = "//*[@id='volvo']/div[3]/form/section/div/div/div[1]/div[2]/div/button";
        public const string prefdlr_searchresult = "//*[@id='volvo']/div[3]/form/section/div/div/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/h6";
        public const string prefdlr_savebtn = "//*[@id='volvo']/div[3]/form/section/div/div/input[3]";
        public const string prefdlr_selecteddlr = "//*[@id='volvo']/div[3]/section/div[2]/ul/li[1]";
        //TS4
        public const string PRofsetting_hdr = "//*[@id='volvo']/h2";
        public const string PRofsetting_hdr_val = "Profile and Settings";
        public const string prof_editvolvoid_link = "//*[@id='volvo']/div[3]/div/div[3]/a[1]";
        public const string prof_changepwd_link = "//*[@id='volvo']/div[3]/div/div[3]/a[2]";
        public const string prof_deletevolvoid_link = "//*[@id='volvo']/div[3]/div/div[3]/a[3]";
        public const string prof_myprofilehdr = "//*[@id='volvo']/div[4]/h3";
        public const string prof_myprofilehdr_val = "My Profile";
        public const string prof_title_dropdown = "//*[@id='ProfileItems_0__Value']";
        public const string prof_FirstName_textbox = "//*[@id='ProfileItems_1__Value']";
        public const string prof_Lastname_textbox = "//*[@id='ProfileItems_2__Value']";
        public const string prof_homeaddr_hdr = "//*[@id='volvo']/div[4]/form/div[4]/h3/span";
        public const string prof_homeaddr_hdr_val = "Home Address";
        public const string prof_Line1_txtbox = "//*[@id='ProfileItems_3__Members_0__Value']";
        public const string prof_Line2_txtbox = "//*[@id='ProfileItems_3__Members_1__Value']";
        public const string prof_Line3_txtbox = "//*[@id='ProfileItems_3__Members_2__Value']";
        public const string prof_Line4_txtbox = "//*[@id='ProfileItems_3__Members_3__Value']";
        public const string prof_postcode_txtbox = "//*[@id='ProfileItems_3__Members_4__Value']";
        public const string prof_city_txtbox = "//*[@id='ProfileItems_3__Members_5__Value']";
        public const string prof_Country_dropdown = "//*[@id='ProfileItems_3__Members_6__Value']";
        public const string prof_phone_txtbox = "//*[@id='ProfileItems_4__Value']";
        public const string prof_mobile_txtbox = "//*[@id='ProfileItems_5__Value']";
        public const string prof_email_txtbox = "//*[@id='ProfileItems_6__Value']";
        public const string prof_prefcommuni_hdr = "//*[@id='volvo']/div[4]/form/div[8]/h3/span";
        public const string prof_prefcommuni_hdr_val = "Contact Preferences";
        public const string prof_email_radio = "//*[@id='volvo']/div[4]/form/div[8]/div[2]/div[2]";
        public const string prof_phone_radio = "//*[@id='volvo']/div[4]/form/div[8]/div[3]/div[2]";
        public const string prof_Directmail_radio = "//*[@id='volvo']/div[4]/form/div[8]/div[4]/div[2]";
        public const string prof_sms_radio = "//*[@id='volvo']/div[4]/form/div[8]/div[5]/div[2]";
        public const string LXP_profset_savebtn = "//*[@id='volvo']/div[4]/form/div[9]/input";
        public const string LXP_prof_titlerror = "//*[@id='ProfileItems_0__Value-error']";
        public const string LXP_prof_titlerror_val = "Please select title";
        public const string LXP_prof_countryerror = "//*[@id='ProfileItems_3__Members_6__Value-error']";
        public const string LXP_prof_countryerror_val = "Please select country";
        public const string LXP_prof_phoneerror = "//*[@id='ProfileItems_5__Value-error']";
        public const string LXP_prof_phoneerror_val = "Not a valid phone number, valid symbols are digits,";
        #endregion

        #region CarLocator
        //TS1
        public const string extendedInventoryButton = "//*[@id='volvo']/div[4]/div/div[3]/div/div[1]/div[1]/div[2]/button";
        public const string inventoryMenu = "//*[@id='v2-mob']/div[2]/div[1]/a";
        public const string inventorySearchbar = "//*[@id='volvo']/div[4]/div/div/div[1]/div/form/input";
        public const string inventorySearchButton = ".//*[@id='volvo']/div[4]/div/div/div[1]/div/form/button";
        public const string inventorysearchresult = "//*[@id='volvo']/div[4]/div/div[3]/div/div[1]/div[1]/div[1]/button/span[2]";
        //Ts2
        public const string selectGridCarLocator = "//*[@id='volvo']/div[4]/div/div[4]/div/ul/li[3]/div/div/div[2]/a";
        public const string exteriorImage1 = ".//*[@id='exteriorImage1']";
        public const string exteriorImage2 = ".//*[@id='exteriorImage2']";
        public const string exteriorImage3 = ".//*[@id='exteriorImage3']";
        public const string exteriorImage4 = ".//*[@id='exteriorImage4']";
        public const string navigateRightArrow = ".//*[@id='volvo']/div[4]/div/div[2]/div[1]/div[1]/div/div[2]/div[2]/div[2]";
        public const string navigateLeftArrow = ".//*[@id='volvo']/div[4]/div/div[2]/div[1]/div[1]/div/div[2]/div[2]/div[1]";
        public const string getAQuoteText = ".//*[@id='volvo']/div[4]/div/div[2]/div[2]/div[1]/div[1]/p[1]";
        public const string ModelYear = "//*[@id='volvo']/div[4]/div/div[2]/div[1]/ul/li[1]";
        public const string FuelEconomy = "//*[@id='volvo']/div[4]/div/div[2]/div[1]/ul/li[2]";
        public const string DriveTrain = "//*[@id='volvo']/div[4]/div/div[2]/div[1]/ul/li[3]";
        public const string ExteriorColour = "//*[@id='volvo']/div[4]/div/div[2]/div[1]/ul/li[4]";
        public const string InteriorColour = "//*[@id='volvo']/div[4]/div/div[2]/div[1]/ul/li[5]";
        public const string VIN = "//*[@id='volvo']/div[4]/div/div[2]/div[1]/ul/li[6]";
        public const string FirstNameTextbox = "//*[@id='txtFirstName']";
        public const string LastNameTextbox = "//*[@id='txtLastName']";
        public const string StreetAddressTextbox = "//*[@id='txtCity']";
        public const string CityTextbox = "//*[@id='txtCity']";
        public const string StateDropDown = "//*[@id='cmbState']";
        public const string ZipcodeTextbox = "//*[@id='txtZipcode']";
        public const string EmailTextbox = "//*[@id='txtEmail']";
        public const string PhoneTextbox = "//*[@id='txtPhone']";
        public const string SendToDealerButton = "//*[@id='volvo']/div[4]/div/div[2]/div[2]/div[1]/form/div[2]/button";
        public const string RequestSentText = "/html/body/div[5]/div/div/div/h2"; 
        public const string CloseButton = "//*[@id='btnSubmit']";
        public const string ConfirmationMessage = "//*[@id='volvo']/div[4]/div/div[2]/div[2]/div[1]/div[2]/p";

        #endregion

        #region DcomItaly
        public const string selectYourVolvo = "//*[@id='itemList_{FA6A50CF-F338-4875-A038-4AFADC602590}']/ul/li[6]/a[2]";
        public const string heroContentXC60 = "//*[@id='standardHero_{96E2D6AD-B443-4135-B187-4BDD43C606A0}']/div[2]";
        public const string heroContentV90CC = "//*[@id='standardHero_{932283A6-5B80-4030-A354-881C202206F9}']/div[2]";
        public const string heroContentXC90 = "//*[@id='standardHero_{C6C627C2-174E-448A-8C56-3F1BAF096B01}']/div[2]";
        public const string title_XC60 = "//*[@id='standardHero_{96E2D6AD-B443-4135-B187-4BDD43C606A0}']/div[2]/div/div/h3";
        public const string title_V90CC = "//*[@id='standardHero_{932283A6-5B80-4030-A354-881C202206F9}']/div[2]/div/div/h3";
        public const string title_XC90 = "//*[@id='standardHero_{C6C627C2-174E-448A-8C56-3F1BAF096B01}']/div[2]/div/div/h3";

        public const string xc60OrderOnline_btn = "//*[@id='standardHero_{96E2D6AD-B443-4135-B187-4BDD43C606A0}']/div[2]/div/div/ul/li/a";
        public const string xc60OrderOnlineText = "//*[@id='standardHero_{96E2D6AD-B443-4135-B187-4BDD43C606A0}']/div[2]/div/div/ul/li/a";
        public const string v90ccOrderOnline_btn = "//*[@id='standardHero_{932283A6-5B80-4030-A354-881C202206F9}']/div[2]/div/div/ul/li/a";
        public const string v90ccOrderOnlineText = "//*[@id='standardHero_{932283A6-5B80-4030-A354-881C202206F9}']/div[2]/div/div/ul/li/a";
        public const string xc90OrderOnline_btn = "//*[@id='standardHero_{C6C627C2-174E-448A-8C56-3F1BAF096B01}']/div[2]/div/div/ul/li/a";
        public const string xc90OrderOnlineText = "//*[@id='standardHero_{C6C627C2-174E-448A-8C56-3F1BAF096B01}']/div[2]/div/div/ul/li/a";

        public const string DCOMPageReturn_btn = "//*[@id='new-secondary-navigation']/div/div/div[1]/div[1]/a";
        public const string DCOMPageSubHeader_1 = "//*[@id='new-secondary-navigation']/div/div/div[2]/div/div[1]/a";
        public const string DCOMPageSubHeader_2 = "//*[@id='new-secondary-navigation']/div/div/div[2]/div/div[2]/a";
        public const string DCOMPageSubHeader_3 = "//*[@id='new-secondary-navigation']/div/div/div[2]/div/div[3]/a";
        public const string DCOMPageSubHeader_4 = "//*[@id='new-secondary-navigation']/div/div/div[2]/div/div[4]/a";

        public const string xc60RentalPageCarPageImage = "/html/body/main/div/form/section[1]/div/div[1]/img";
        public const string xc60RentalPageCarInformation = "/html/body/main/div/form/section[1]/div/div[2]/div/div[1]";
        public const string xc60RentalPagePriceInformation = "/html/body/main/div/form/section[1]/div/div[2]/div/div[2]";
        public const string xc60RentalPageOfferSummary_link = "//*[@id='car-pdflink']";
        

        public const string xc60RentalPageRentalDetailsTitle = "/html/body/main/div/form/section[2]/div/section/h2";
        public const string xc60RentalPageRentalDeatilsPrice = "/html/body/main/div/form/section[2]/div/div/div[1]";
        public const string xc60RentalPageRentalDeatilsParagragh = "/html/body/main/div/form/section[2]/div/div/div[2]";
        public const string xc60RentalPageRentalDetailsEdit_btn = "/html/body/main/div/form/section[2]/div/div/div[3]/div";

        public const string xc60RentalPageServicesIncludedTitle = "/html/body/main/div/form/section[3]/h1";
        public const string xc60RentalPageServicesIncludedTitle_1 = "/html/body/main/div/form/section[3]/div/div[1]/div[2]/div/div/h2";
        public const string xc60RentalPageServicesIncludedTitle_2 = "/html/body/main/div/form/section[3]/div/div[2]/div[2]/div/div/h2";
        public const string xc60RentalPageServicesIncludedTitle_3 = "/html/body/main/div/form/section[3]/div/div[3]/div[2]/div/div/h2";
        public const string xc60RentalPageServicesIncludedTitle_4 = "/html/body/main/div/form/section[3]/div/div[4]/div[2]/div/div/h2";
        public const string xc60RentalPageServicesIncludedTitle_5 = "/html/body/main/div/form/section[3]/div/div[5]/div[2]/div/div/h2";
        public const string xc60RentalPageServicesIncludedImage_1 = "/html/body/main/div/form/section[3]/div/div[1]/div[1]/img";
        public const string xc60RentalPageServicesIncludedImage_2 = "/html/body/main/div/form/section[3]/div/div[2]/div[1]/img";
        public const string xc60RentalPageServicesIncludedImage_3 = "/html/body/main/div/form/section[3]/div/div[3]/div[1]/img";
        public const string xc60RentalPageServicesIncludedImage_4 = "/html/body/main/div/form/section[3]/div/div[4]/div[1]/img";
        public const string xc60RentalPageServicesIncludedImage_5 = "/html/body/main/div/form/section[3]/div/div[5]/div[1]/img";

        public const string xc60RentalPageEquipmentIncludedTitle = "/html/body/main/div/form/section[4]/h1";
        public const string xc60RentalPageEquipmentIncludedTitle_1 = "/html/body/main/div/form/section[4]/div/div[1]/div[2]/div/div/h2";
        public const string xc60RentalPageEquipmentIncludedTitle_2 = "/html/body/main/div/form/section[4]/div/div[2]/div[2]/div/div/h2";
        public const string xc60RentalPageEquipmentIncludedTitle_3 = "/html/body/main/div/form/section[4]/div/div[3]/div[2]/div/div/h2";
        public const string xc60RentalPageEquipmentIncludedTitle_4 = "/html/body/main/div/form/section[4]/div/div[4]/div[2]/div/div/h2";
        public const string xc60RentalPageEquipmentIncludedImage_1 = "/html/body/main/div/form/section[4]/div/div[1]/div[1]/img";
        public const string xc60RentalPageEquipmentIncludedImage_2 = "/html/body/main/div/form/section[4]/div/div[2]/div[1]/img";
        public const string xc60RentalPageEquipmentIncludedImage_3 = "/html/body/main/div/form/section[4]/div/div[3]/div[1]/img";
        public const string xc60RentalPageEquipmentIncludedImage_4 = "/html/body/main/div/form/section[4]/div/div[4]/div[1]/img";
        public const string xc60RentalPageContinueButton = "/html/body/main/div/form/section[5]/div/div/button";

        public const string selectYourVolvo_txt = "SELEZIONA LA TUA VOLVO";
        public const string title_XC60_txt = "VOLVO XC60";
        public const string title_V90CC_txt = "VOLVO V90 CROSS COUNTRY";
        public const string title_XC90_txt = "VOLVO XC90";
        public const string orderOnlineBtn_txt = "ORDINA ONLINE";
        public const string RentalPageRentalDetailsTitle_txt = "Dettagli del noleggio";
        public const string RentalPageEquipmentIncludedTitle_txt = "Equipaggiamento incluso";
        public const string RentalPageServicesIncludedTitle_XC60_txt = "Servizi inclusi";

        public const string xc60FormPageCarImage = "//*[@id='bigFormData']/section/div/div[1]/img";
        public const string xc60FormPageCarInformation = "//*[@id='bigFormData']/section/div/div[2]/div/div[1]";
        public const string xc60FormPagePriceInformation = "//*[@id='bigFormData']/section/div/div[2]/div/div[2]";
        public const string xc60FormPageOfferSummary_link = "//*[@id='car-pdflink']";

        public const string xc60FormPageInsertYourDataTitle = "//*[@id='bigFormData']/div[1]/div/div[1]";
        public const string xc60FormPageTitle_label = "//*[@id='bigFormData']/div[1]/div/div[2]/div/div/label";
        public const string xc60FormPageName_label = "//*[@id='bigFormData']/div[1]/div/div[3]/div/div[1]/label";
        public const string xc60FormPageSurname_label = "//*[@id='bigFormData']/div[1]/div/div[3]/div/div[2]/label";
        public const string xc60FormPageEmail_label = "//*[@id='bigFormData']/div[1]/div/div[5]/div/div[1]/label";
        public const string xc60FormPageConfirmEmail_label = "//*[@id='bigFormData']/div[1]/div/div[5]/div/div[2]/label";
        public const string xc60FormPageAddress_label = "//*[@id='bigFormData']/div[1]/div/div[7]/div/div/label";
        public const string xc60FormPageLocation_label = "//*[@id='bigFormData']/div[1]/div/div[8]/div/div[1]/label";
        public const string xc60FormPageProvince_label = "//*[@id='bigFormData']/div[1]/div/div[8]/div/div[2]/label";
        public const string xc60FormPagePostalCode_label = "//*[@id='bigFormData']/div[1]/div/div[9]/div/div[1]/label";
        public const string xc60FormPageNation_label = "//*[@id='bigFormData']/div[1]/div/div[9]/div/div[2]/label";
        public const string xc60FormPageFixedTel_label = "//*[@id='bigFormData']/div[1]/div/div[10]/div/div[1]/label";
        public const string xc60FormPageCellPhone_label = "//*[@id='bigFormData']/div[1]/div/div[10]/div/div[2]/label";

        public const string xc60FormPageTitle_dropdown = "//*[@id='bigFormData']/div[1]/div/div[2]/div/div/select";
        public const string xc60FormPageName_txtbox = "//*[@id='bigFormData']/div[1]/div/div[3]/div/div[1]/input";
        public const string xc60FormPageSurname_txtbox = "//*[@id='bigFormData']/div[1]/div/div[3]/div/div[2]/input";
        public const string xc60FormPageEmail_txtbox = "//*[@id='bigFormData']/div[1]/div/div[5]/div/div[1]/input";
        public const string xc60FormPageConfirmEmail_txtbox = "//*[@id='bigFormData']/div[1]/div/div[5]/div/div[2]/input";
        public const string xc60FormPageAddress_txtbox = "//*[@id='bigFormData']/div[1]/div/div[7]/div/div/input";
        public const string xc60FormPageLocation_txtbox = "//*[@id='bigFormData']/div[1]/div/div[8]/div/div[1]/input";
        public const string xc60FormPageProvince_txtbox = "//*[@id='bigFormData']/div[1]/div/div[8]/div/div[2]/input";
        public const string xc60FormPagePostalCode_txtbox = "//*[@id='bigFormData']/div[1]/div/div[9]/div/div[1]/input";
        public const string xc60FormPageNation_dropdown = "//*[@id='bigFormData']/div[1]/div/div[9]/div/div[2]/select";
        public const string xc60FormPageFixedTel_txtbox = "//*[@id='bigFormData']/div[1]/div/div[10]/div/div[1]/input";
        public const string xc60FormPageCellPhone_txtbox = "//*[@id='bigFormData']/div[1]/div/div[10]/div/div[2]/input";

        public const string xc60FormPageStayUptoDateTitle = "//*[@id='bigFormData']/div[1]/div/div[11]/h2";
        public const string xc60FormPageStayUptoDateEmail_label = "//*[@id='bigFormData']/div[1]/div/div[13]/div/div/div[2]/div[1]/div/div[1]/span";
        public const string xc60FormPageStayUptoDateMail_label = "//*[@id='bigFormData']/div[1]/div/div[13]/div/div/div[2]/div[2]/div/div[1]/span";
        public const string xc60FormPageStayUptoDatePhone_label = "//*[@id='bigFormData']/div[1]/div/div[13]/div/div/div[2]/div[3]/div/div[1]/span";
        public const string xc60FormPageStayUptoDateSMS_label = "//*[@id='bigFormData']/div[1]/div/div[13]/div/div/div[2]/div[4]/div/div[1]/span";

        public const string xc60FormPageStayUptoDateEmail_radiobtn_Y = "//*[@id='bigFormData']/div[1]/div/div[13]/div/div/div[2]/div[1]/div/div[2]/label";
        public const string xc60FormPageStayUptoDateMail_radiobtn_Y = "//*[@id='bigFormData']/div[1]/div/div[13]/div/div/div[2]/div[2]/div/div[2]/label";
        public const string xc60FormPageStayUptoDatePhone_radiobtn_Y = "//*[@id='bigFormData']/div[1]/div/div[13]/div/div/div[2]/div[3]/div/div[2]/label";
        public const string xc60FormPageStayUptoDateSMS_radiobtn_Y = "//*[@id='bigFormData']/div[1]/div/div[13]/div/div/div[2]/div[4]/div/div[2]/label";
        public const string xc60FormPageStayUptoDateEmail_radiobtn_N = "//*[@id='bigFormData']/div[1]/div/div[13]/div/div/div[2]/div[1]/div/div[3]/label";
        public const string xc60FormPageStayUptoDateMail_radiobtn_N = "//*[@id='bigFormData']/div[1]/div/div[13]/div/div/div[2]/div[2]/div/div[3]/label";
        public const string xc60FormPageStayUptoDatePhone_radiobtn_N = "//*[@id='bigFormData']/div[1]/div/div[13]/div/div/div[2]/div[3]/div/div[3]/label";
        public const string xc60FormPageStayUptoDateSMS_radiobtn_N = "//*[@id='bigFormData']/div[1]/div/div[13]/div/div/div[2]/div[4]/div/div[3]/label";

        public const string xc60FormPageDealer_Table = "//*[@id='orderpreview']/div[2]/ul";
        public const string xc60FormPageDealer_Map = "//*[@id='orderpreview']/div[3]/div";
        public const string xc60FormPageSelectYourDealerTitle = "//*[@id='bigFormData']/div[2]/div[1]/h1";
        public const string xc60FormPageDealer_txtbox = "//*[@id='dealer-locator-search']";
        public const string xc60FormPageDealer_OK_btn = "//*[@id='search-btn']";
        public const string xc60FormPageDealerName_dealer1 = "//*[@id='orderpreview']/div[2]/ul/li[1]/a/table/tbody/tr/td[1]/table/tbody/tr/td/div/p[1]";
        public const string xc60FormPageContinueCTA = "//*[@id='bigFormSubmit']";

        public const string xc60SubmitOrderPageCarImage = "//*[@id='summarydata']/div[3]/section[1]/div/div[1]/img";
        public const string xc60SubmitOrderPageCarInformation = "//*[@id='summarydata']/div[3]/section[1]/div/div[2]/div/div[1]";
        public const string xc60SubmitOrderPagePriceInformation = "//*[@id='summarydata']/div[3]/section[1]/div/div[2]/div/div[2]";
        public const string xc60SubmitOrderPageOfferSummary_link = "//*[@id='car-pdflink']";

        public const string xc60SubmitOrderPageReviewRentalDataTitle = "//*[@id='summarydata']/div[3]/section[2]/h2/font/font";
        public const string xc60SubmitOrderPageYourDataInformation = "//*[@id='summarydata']/div[3]/div[1]";
        public const string xc60SubmitOrderPageYourDealerInformation = "//*[@id='summarydata']/div[3]/div[2]";
        public const string xc60SubmitOrderPageRentalInformation = "//*[@id='summarydata']/div[3]/section[3]";
        public const string xc60SubmitOrderPageAcceptCTA = "//*[@id='F493A58FFFE04B58BC3A8C1DCD5BF0C1']/div[2]/div[2]/button";
        public const string xc60SubmitOrderPageTermsAndConditionsCTA = "//*[@id='bigFormData']/div/div/label";
        public const string xc60SubmitOrderPageCaptchaCheckbox = "//*[@id='recaptcha-anchor']/div[5]";
        #endregion

        #region Dcom Accessories
        public const string uk_continue_button = "//*[@id='volvo']/div[3]/div/ul/li[3]/button";
        public const string uk_contactus_link = "//*[@id='volvo']/div[3]/ul/li[1]";
        public const string uk_faq_link = "//*[@id='volvo']/div[3]/ul/li[2]";

        public const string uk_backgroundimage = "//*[@id='standardHero_{6F001427-F57B-4407-A18A-1AAC38242564}']/div[1]/img";
        public const string uk_txt_volvoacc = "//*[@id='standardHero_{6F001427-F57B-4407-A18A-1AAC38242564}']/div[2]/div/div/h1";
        public const string uk_Accessories_text_volvoaccheader = "Volvocars Accessories";
        public const string uk_txt_volvoBodytext = "//*[@id='standardHero_{6F001427-F57B-4407-A18A-1AAC38242564}']/div[2]/div/div/div/p";
        public const string uk_txt_autoaccesories = "//div[@id='volvo']/div[2]/div[2]/div[2]/div/div/h1";
        public const string uk_Accessories_text_Autoaccessories = "Car Accessories";
        public const string uk_txt_lifestylecollect = "//div[@id='volvo']/div[2]/div[3]/div[2]/div/div/h1";
        public const string uk_Accessories_text_Lifestylecollect = "Lifestyle collection";

        public const string uk_img_autoaccesoriesimg = "//div[@id='volvo']/div[2]/div[2]/div/img";
        public const string uk_txt_autoaccesoriesbody = "//div[@id='volvo']/div[2]/div[2]/div[2]/div/div/div/P";
        public const string uk_btn_autoacc_collectionbutton = "//div[@id='volvo']/div[2]/div[2]/div[2]/div/div/ul/li/a";
        public const string uk_txt_autoaccheader = "//*[@id='volvo']/div[3]/div[2]/div/h1";
        public const string uk_Accessories_text_autoaccpage_header = "Car Accessory Sales";

        public const string uk_img_lifestyleimg = "//div[@id='volvo']/div[2]/div[3]/div/img";
        public const string uk_txt_lifestylebody = "//div[@id='volvo']/div[2]/div[3]/div[2]/div/div/div/p[2]";
        public const string uk_btn_lifestyle_collectionbutton = "//div[@id='volvo']/div[2]/div[3]/div[2]/div/div/ul/li/a";
        public const string uk_txt_lifestylepgheader = "//*[@id='volvo']/div[3]/div[2]/div/h1";
        public const string uk_Accessories_text_lifestylecolpage_header = "Lifestyle & Accessories";

        public string uk_lbl_navigation_accessories = "//*[@id='acc-heading-btn']/h4/a";
        public string uk_lbl_navigation_Categories = "//*[@id='subNavSearchController']/div[1]/div/div/div[3]/div/div[2]/ul/li";
        public string uk_lbl_navigation_select_dealer = "//*[@id='subNavSearchController']/div[1]/div/div/div[3]/div/div[2]/ul/li[2]/ul/li[1]";
        public string uk_lbl_navigation_select_model = "//*[@id='subNavSearchController']/div[1]/div/div/div[3]/div/div[2]/ul/li[3]/ul/li[1]";
        public string uk_txt_navigation_Search = "//*[@id='search']";
        public const string uk_basketicon = "//*[@id='shopping-basket-desktop']/a/i";

        public string uk_selectdealer_title = "//*[@id='subNavSearchController']/div[3]/div/div/div[2]/div[1]/div/h2";
        public string uk_searchdealer_textbox = "//*[@id='dealer-locator-search']";
        public string uk_dealer_accordiantable = "//*[@id='find-a-dealer']/div/div[2]/div[1]/div/ul";
        public string uk_dealer_selectbutton = "//*[@id='find-a-dealer']/div/div[2]/div[1]/div/ul/li[1]/div[1]/div[3]/button";
        public string uk_dealermap = "//*[@id='find-a-dealer']/div/div[2]/div[2]";

        public string uk_carselector_saloon = "//*[@id='carCategoryTabs']/div/div[1]";
        public string uk_carselector_estate = "//*[@id='carCategoryTabs']/div/div[2]";
        public string uk_carselector_suv = "//*[@id='carCategoryTabs']/div/div[3]";
        public string uk_carselector_saloon_select = "//*[@id='carCategoryTabs']/div/div[1]/ul/li[2]/div/button";

        public string uk_carselector_s60 = "//*[@id='tab-1']/div/div[1]";
        public string uk_carselector_s60cc = "//*[@id='tab-1']/div/div[2]";
        public string uk_carselector_s60_ModelYear = "//*[@id='tab-1']/div/div[1]/div/div/span[1]";
        public string uk_carselector_s60_ModelYear_DD = "//*[@id='tab-1']/div/div[1]/div/ul";
        public string uk_carselector_s60_selectbutton = "//*[@id='accCarSelector']";

        public string uk_searchResults_sidebar = "//*[@id='volvo']/div[3]/div[3]/div/div[1]/div";
        public string uk_searchresults_sidebar_accessories = "//*[@id='volvo']/div[3]/div[3]/div/div[1]/div/div[4]/ul/li[1]";
        public string uk_searchresults_sidebar_lifestyle = "//*[@id='volvo']/div[3]/div[3]/div/div[1]/div/div[4]/ul/li[2]";
        public string uk_searchresults_list = "//*[@id='acc-pagination-scroll']/div[2]";
        public string uk_searchresults_viewitem = "//*[@id='acc-pagination-scroll']/div[2]/div[1]/div[1]/div/div/div/div[2]/div/p[2]/a";

        public string uk_pdp_img = "//*[@id='acc-gallery-carousel']/div/div/figure/img";
        public string uk_pdp_backlink = "//*[@id='volvo']/div[3]/div/div[1]/div/div/a";
        public string uk_pdp_productdetailSection = "//*[@id='volvo']/div[3]/div/div[2]/div/div[2]";
        public string uk_pdp_producttitle = "//*[@id='volvo']/div[3]/div/div[2]/div/div[2]/div[1]/h1";
        public string uk_pdp_pickupbutton = "//*[@id='volvo']/div[3]/div/div[2]/div/div[2]/div[4]/div/div/div/div[3]/span/label";
        public string uk_pdp_moreinfolink = "//*[@id='special']";
        public string uk_pdp_pricedetails = "//*[@id='volvo']/div[3]/div/div[2]/div/div[2]/div[5]/div/div/h1";
        public string uk_pdp_Addtobasketbutton = "//*[@id='volvo']/div[3]/div/div[2]/div/div[2]/div[6]/div/div/div/div[2]/div/div[2]/div/button";
        public string uk_pdp_quantity_dropdown = "//*[@id='volvo']/div[3]/div/div[2]/div/div[2]/div[6]/div/div/div/div[2]/div/div[1]/div/div/span[1]";
        public string uk_pdp_productdescription = "//*[@id='volvo']/div[3]/div/div[3]";
        public string uk_pdp_productspec = "//*[@id='volvo']/div[3]/div/div[4]";

        public const string uk_basket_continueshoppinglink = "//*[@id='volvo']/div[3]/div/div[2]/div/div/a";
        public const string uk_basket_title = "//*[@id='volvo']/div[3]/div/div[3]/div[1]/div/div/h1";
        public const string uk_basket_item = "//*[@id='volvo']/div[3]/div/div[3]/div[2]/div/div[3]/div";
        public const string uk_ordersummary = "//*[@id='volvo']/div[3]/div/div[3]/div[3]";
        public const string uk_checkout_button = "//*[@id='volvo']/div[3]/div/div[3]/div[4]/div/div/div/button";

        public const string uk_backtobasket_link = "//*[@id='checkoutConfirmationController']/div[1]/div/div[1]/div/div[1]/div/a";
        public const string uk_checkout_title = "//*[@id='checkoutConfirmationController']/div[1]/div/div[2]/div/h1";
        public const string uk_checkout_contactdetails_title = "//*[@id='checkoutConfirmationController']/div[2]/div[1]/div/div[1]/h2";
        public const string uk_checkout_namelabel = "//*[@id='checkoutConfirmationController']/div[2]/div[3]/div/form/div[1]/div/div/div[1]/div[1]/div[1]/label";
        public const string uk_checkout_firstname = "//*[@id='DealerPickup_FirstName']";
        public const string uk_checkout_lastname = "//*[@id='DealerPickup_LastName']";
        public const string uk_checkout_emaillabel = "//*[@id='checkoutConfirmationController']/div[2]/div[3]/div/form/div[1]/div/div/div[2]/div[1]/div[1]/label";
        public const string uk_checkout_email = "//*[@id='DealerPickup_FirstEmail']";
        public const string uk_checkout_confirmemail = "//*[@id='DealerPickup_ConfirmedEmail']";
        public const string uk_checkout_phonenumberlabel = "//*[@id='checkoutConfirmationController']/div[2]/div[3]/div/form/div[1]/div/div/div[3]/div[1]/label";
        public const string uk_checkout_phonenumber = "//*[@id='DealerPickup_PhoneNumber']";
        public const string uk_checkout_preferredcontact_label = "//*[@id='checkoutConfirmationController']/div[2]/div[3]/div/form/div[1]/div/div/div[4]/div[1]/label";
        public const string uk_checkout_email_button = "//*[@id='checkoutConfirmationController']/div[2]/div[3]/div/form/div[1]/div/div/div[4]/div[2]/ul/li[1]";
        public const string uk_checkout_phone_button = "//*[@id='checkoutConfirmationController']/div[2]/div[3]/div/form/div[1]/div/div/div[4]/div[2]/ul/li[2]";
        public const string uk_checkout_regnumber_label = "//*[@id='checkoutConfirmationController']/div[2]/div[3]/div/form/div[1]/div/div/div[5]/div[1]/label";
        public const string uk_checkout_regnumber = "//*[@id='DealerPickup_CarRegistrationNumber']";
        public const string uk_checkout_captcha = "/html/body/div[2]";
        public const string uk_checkout_item = "//*[@id='checkoutConfirmationController']/div[2]/div[3]/div/div[2]";
        #endregion

        #region DcomReularSales
        public const string careByVolvoHomePageHeroContent = "//*[@id='standardHero_{FE58D8B8-ABB0-4269-B88D-9046059A0DF2}']/div[2]";
        public const string careByVolvoHomePageCareByVolvoTitle = "//*[@id='standardHero_{FE58D8B8-ABB0-4269-B88D-9046059A0DF2}']/div[2]/div/div/h1";
        public const string careByVolvoHomePageCareByVolvoTitle_txt = "CARE BY VOLVO";
        public const string careByVolvoHomePageIndicationBar = "/html/body/main/div[1]/div[3]";
        public const string careByVolvoHomePageHeadertextsection1 = "/html/body/main/section[1]";
        public const string careByVolvoHomePageDealersection = "/html/body/main/section[2]";
        public const string careByVolvoHomePageHeadertextsection2 = "/html/body/main/section[3]";
        public const string careByVolvoHomePageExfeature = "//*[@id='exteriorFeatureTwo_{33A7D247-38FC-44BD-9E37-994F746B802E}']";
        public const string careByVolvoHomePageJourneyContainer = "//*[@id='content']";
        public const string careByVolvoHomePageCallChatSection = "/html/body/main/section[6]";

        public const string careByVolvoHomePageDealerTitle = "/html/body/main/section[2]/div/section/h2";
        public const string careByVolvoHomePageDealerMarker = "/html/body/main/section[2]/div/div/div[1]/div";
        public const string careByVolvoHomePageDealerNameContainer = "/html/body/main/section[2]/div/div/div[2]";
        public const string careByVolvoHomePageChooseDealer_btn = "//*[@class='button D37EBE630E7F4B2B9936A404B0CEB82E_open']";
        public const string careByVolvoHomePageSelectedDealerName = "/html/body/main/section[2]/div/div/div[1]/div/div[2]/p[1]";

        public const string careByVolvoHomePageDealerOverLayTitle = "//*[@id='D37EBE630E7F4B2B9936A404B0CEB82E']/section/h2";
        public const string careByVolvoHomePageDealerOverLay_txtbox = "//*[@id='dealer-locator-search']";
        public const string careByVolvoHomePageDealerOverLaySearch_btn = "//*[@id='search-btn']";
        public const string careByVolvoHomePageDealerOverLayDealerTable = "//*[@id='orderpreview']/div[2]/ul";
        public const string careByVolvoHomePageDealerOverLayDealer1 = "//*[@id='orderpreview']/div[2]/ul/li/a/div[1]";
        public const string careByVolvoHomePageDealerOverLayDealer2 = "//*[@id='orderpreview']/div[2]/ul/li[2]";
        public const string careByVolvoHomePageDealerOverLayCloseIcon_top = "//*[@id='D37EBE630E7F4B2B9936A404B0CEB82E']/div[1]";
        public const string careByVolvoHomePageDealerOverLayCloseButton = "//*[@id='D37EBE630E7F4B2B9936A404B0CEB82E']/div[4]/div[1]/span";
        public const string careByVolvoHomePageDealerOverLayAcceptButton = "//*[@id='D37EBE630E7F4B2B9936A404B0CEB82E']/div[4]/div[2]/button";
        public const string careByVolvoHomePageDealerOverLayDealer1Name = "//*[@id='orderpreview']/div[2]/ul/li/a/div[2]";

        public const string careByVolvoHomePageDealerTitle_txt = "Din valgte forhandler";
        public const string careByVolvoHomePageDealerOverLayTitle_txt = "Bytt forhandler";

        public const string careByVolvoHomePageDealerContinueButton = "/html/body/main/section[4]/div/div/button";
        public const string careByvolvoBuildXC40 = "//*[@id='vbs-page__content']/div/div[2]/div/div[1]/ul/li[3]";
        public const string careByVolvoBuildOkButton = "//*[@id='vbs-body']/div[6]/div/div/div[3]/button[3]";

        public const string careByVolvoBuildTrim1 = "//*[@id='vbs-page__content']/div/div[1]/div/div/div[1]";
        public const string careByVolvoBuildTrim2 = "//*[@id='vbs-page__content']/div/div[1]/div/div/div[2]";

        public const string careByVolvoBuildEngineTab = "//*[@id='vbs-header']/div[1]/div/ul/li[3]/a/span";
        public const string careByVolvoBuildEngine1 = "//*[@id='vbs-page__content']/div/div[3]/div/div/div[1]";
        public const string careByVolvoBuildEngine2 = "//*[@id='vbs-page__content']/div/div[3]/div/div/div[2]";

        public const string careByVolvoBuildDesignTab = "//*[@id='vbs-header']/div[1]/div/ul/li[4]/a/span";
        public const string careByVolvoBuildDesignVisualizationsection = "//*[@id='vbs-visualization']";
        public const string careByVolvoBuildDesignSelectionsection = "//*[@id='vbs-page']";
        public const string careByVolvoBuildDesignSidebarsection = "//*[@id='vbs-sidebar']";

        public const string careByVolvoBuildPacksTab = "//*[@id='vbs-header']/div[1]/div/ul/li[5]/a/span";
        public const string careByVolvoBuildPack1 = "//*[@id='vbs-page__content']/div/div[2]/div/div/div/div/div[1]";
        public const string careByVolvoBuildPack2 = "//*[@id='vbs-page__content']/div/div[2]/div/div/div/div/div[2]";
        public const string careByVolvoBuildPack3 = "//*[@id='vbs-page__content']/div/div[2]/div/div/div/div/div[3]";
        public const string careByvolvoBuildPack4 = "//*[@id='vbs-page__content']/div/div[2]/div/div/div/div/div[4]";
        public const string careByvolvoBuildPack5 = "//*[@id='vbs-page__content']/div/div[2]/div/div/div/div/div[5]";

        public const string careByVolvoBuildSummaryTab = "//*[@id='vbs-header']/div[1]/div/ul/li[6]/a/span";
        public const string careByVolvoBuildSummaryVisualization = "//*[@id='vbs-summary-visualization']";
        public const string careByVolvoBuildSummaryIntro = "//*[@id='vbs-summary-intro']";
        public const string careByVolvoBuildSummaryDetails = "//*[@id='vbs-summary-details']";
        public const string careByVolvoBuildSummaryFeatures = "//*[@id='vbs-summary-features']";
        public const string careByVolvoBuildSummaryContinueButton = "//*[@id='vbs-summary-intro']/div[3]/div/div/div/button";

        public const string careByVolvoFinancialOptionsBackLink = "/html/body/main/div[1]/div[2]/a/div";
        public const string careByVolvoFinancialOptionsIndicationBar = "/html/body/main/div[1]/div[3]";
        public const string careByVolvoFinancialOptionsCarImage = "/html/body/main/section[1]/div/div[1]";
        public const string careByVolvoFinancialOptionsCarDetails = "/html/body/main/section[1]/div/div[2]";
        public const string careByVolvoFinancialOptionsAgreementSection = "/html/body/main/section[2]";
        public const string careByVolvoFinancialOptionsAppointmentInformation = "/html/body/main/section[3]";
        public const string careByVolvoFinancialOptionsPricingDetails = "/html/body/main/section[4]/div[1]";
        public const string careByVolvoFinancialOptionsSeeMoreLink = "//*[@id='package-information-slide-toggle-button']";
        public const string careByVolvoFinancialOptionsMorePricingDetails = "/html/body/main/section[4]/div[2]";
        public const string careByVolvoFinancialOptionsSeeLessLink = "//*[@id='package-information-slide-toggle-button']";
        public const string careByVolvoFinancialOptionsLoginHeader = "/html/body/main/div[2]/section/h2";
        public const string careByVolvoFinancialOptionsBankIDButton = "/html/body/main/div[2]/form[1]/button";
        public const string careByVolvoFinancialOptionsMobileBankIDButton = "/html/body/main/div[2]/form[2]/button";
        public const string careByVolvoFinancialOptionsJourneyContainer = "//*[@id='content']";
        public const string careByVolvoFinancialOptionsCallChatSection = "/html/body/main/section[5]";

        #endregion

        #region General sales
        public const string EnterDetailsPageFormTitle = "Your details";
        public const string EnterDetailsPageFirstName_txt = "First name*";
        public const string EnterDetailsPageLastName_txt = "Last name*";
        public const string EnterDetailsPageStreetAddress_txt = "Street address*";
        public const string EnterDetailsPageCity_txt = "City*";
        public const string EnterDetailsPageState_txt = "State*";
        public const string EnterDetailsPageZipCode_txt = "ZIP code*";
        public const string EnterDetailsPageEmailAddress_txt = "Email address*";
        public const string EnterDetailsPageEmailConfirmation_txt = "Email confirmation*";
        public const string EnterDetailsPagePhone_txt = "Phone*";
        public const string EnterDetailsPageRequired_txt = "* Required";
        public const string EnterDetailsPageSelectDealerTitle_txt = "Select preferred dealer";
        public const string EnterDetailsPageBackTobuildFooter_Txt = "Back to build";
        public const string FormInput_name = "test_";
        public const string FormInput_state = "TE";
        public const string FormInput_zipcode = "13101";
        public const string FormInput_email = "test@xyz.com";
        public const string FormInput_phone = "(980) 899-9009";

        public const string s90firstNameTextBox = "//*[@id='customer']/div/div[2]/div[1]/input";
        public const string s90lastNameTextBox = "//*[@id='customer']/div/div[2]/div[2]/input";
        public const string s90streetAddressTextBox = "//*[@id='customer']/div/div[3]/div/input";
        public const string s90cityTextBox = "//*[@id='customer']/div/div[4]/div[1]/input";
        public const string s90stateTextBox = "//*[@id='customer']/div/div[4]/div[2]/input";
        public const string s90zipCodeTextBox = "//*[@id='customer']/div/div[5]/div[1]/input";
        public const string s90emailAddressTextBox = "//*[@id='customer']/div/div[6]/div[1]/input";
        public const string s90emailConfirmationTextBox = "//*[@id='customer']/div/div[6]/div[2]/input";
        public const string s90phoneTextBox = "//*[@id='customer']/div/div[7]/div/input";


        public const string s90OrderPreviewHeader = "//*[@id='volvo']/div[2]/div/div[2]/div/div/h1";
        public const string s90OrderPreviewTitle = "//*[@id='volvo']/div[2]/div/div[3]/div/div/div[1]/div";
        public const string s90OrderPreviewImage = "//*[@id='volvo']/div[2]/div/div[3]/div/div/div[1]/img";
        public const string s90OrderPreviewPriceDetails = "//*[@id='volvo']/div[2]/div/div[3]/div/div/div[2]";

        public const string s90OrderPreviewPreferredDealer = "//*[@id='dealer']";
        public const string s90OrderPreviewDealerMap = "//*[@id='googleMap']";
        public const string s90OrderPreviewSelectedDealer = "//*[@id='dealer']/div/div[2]/div[2]/div/div[1]";
        public const string s90OrderPreviewChangeDealerLink = "//*[@id='dealer']/div/div[2]/div[2]/div/div[1]/div/a";

        public const string s90OrderPreviewDelivery = "//*[@id='volvo']/div[2]/div/div[6]";
        public const string s90OrderPreviewBacktobuild = "//*[@id='volvo']/div[2]/div/div[8]/div/div/div[2]/a";
        public const string s90ContinueToOrderSummaryButton = "//*[@id='volvo']/div[2]/div/div[8]/div/div/div[1]/button";

        public const string s90OrdersummaryHeader = "//*[@id='volvo']/div[2]/div[1]/div[2]/div/div[1]/div/div/h1";
        public const string s90OrdersummaryTitle = "//*[@id='volvo']/div[2]/div[1]/div[2]/div/div[2]/div/div/div[1]/div";
        public const string s90OrdersummaryImage = "//*[@id='volvo']/div[2]/div[1]/div[2]/div/div[2]/div/div/div[1]/img";
        public const string s90OrdersummaryPriceDeatils = "//*[@id='volvo']/div[2]/div[1]/div[2]/div/div[2]/div/div/div[2]";

        public const string s90OrdersummaryPersonalDetails = "//*[@id='volvo']/div[2]/div[1]/div[2]/div/div[3]/div/div[1]";
        public const string s90OrdersummaryDealerDetails = "//*[@id='volvo']/div[2]/div[1]/div[2]/div/div[3]/div/div[2]";
        public const string s90OrdersummaryDelivery = "//*[@id='volvo']/div[2]/div[1]/div[2]/div/div[3]/div/div[3]";

        public const string s90OrdersummaryDepositAndTotalPrice = "//*[@id='volvo']/div[2]/div[1]/div[2]/div/div[4]/div/div";
        public const string s90OrdersummaryPaymentObligation = "//*[@id='checkme1']";
        public const string s90OrdersummaryTermsAndConditions = "//*[@id='checkme2']";
        public const string s90OrderSummaryBackToDetails = "//*[@id='submitorder']/div/div/div[1]/div[1]/a";
        public const string s90OrderSummaryContinueToPaymentButton = "//*[@id='submit1']";

        public const string PaymentPageTitle = "/html/body/div[3]/div[1]/h1";
        public const string PaymentPageCardTitle = "/html/body/div[3]/div[2]/div/div/div[3]/h2";
        public const string PaymentPageForm = "//*[@id='paymentFormCard']/ul";
        public const string PaymentPageConfirmAmt = "//*[@id='paymentFormCard']/div[1]";
        public const string PaymentPagePaymentButton = "//*[@id='paymentFormCard']/div[2]/buttom";
        public const string PaymentPageCancelPaymentLink = "//*[@id='cancelPayment']/button";
        #endregion
    }
}
