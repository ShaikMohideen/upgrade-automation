﻿using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace UpgradeSmoke
{
    public class Methods : ElementRepository
    {
        #region StoryTelling
        public void StoryTellingIntl()
        {
            Click_Element(Discover_Volvo_Menu);
            ValidateMostRecentTab();
            ValidateMostPopularTab();
            ValidateByCarMenuINTL();
            ValidateByTopicMenuINTL();
        }

        public void StoryTellingUK()
        {
            Menu_Click(Discover_Menu);
            Click_Element(Discover_Volvo);
            waitfor(2000);

            ValidateMostRecentTab();
            ValidateMostPopularTab();
            ValidateByTopicMenuUK();
            ValidateByCarMenuUK();
        }

        public void ValidateMostRecentTab()
        {
            bool MostRecentTab_IsActive = IsElementActive(Most_Recent_tab);
            Assert.AreEqual(true, MostRecentTab_IsActive);

            bool MainStoriesModule_RecentTab = IsElementDisplayed(MainStories_Module);
            Assert.AreEqual(true, MainStoriesModule_RecentTab);

            bool TextInfoModule_RecentTab = IsElementDisplayed(TextInfo_Module);
            Assert.AreEqual(true, TextInfoModule_RecentTab);

            bool OtherStoriesModule_RecentTab = IsElementDisplayed(OtherStories_Module);
            Assert.AreEqual(true, OtherStoriesModule_RecentTab);
        }

        public void ValidateMostPopularTab()
        {
            Click_Element(Most_Popular_tab);
            waitfor(2000);

            bool MostPopularTab_IsActive = IsElementActive(Most_Popular_tab);
            Assert.AreEqual(true, MostPopularTab_IsActive);

            bool MainStoriesModule_PopularTab = IsElementDisplayed(MainStories_Module);
            Assert.AreEqual(true, MainStoriesModule_PopularTab);

            bool TextInfoModule_PopularTab = IsElementDisplayed(TextInfo_Module);
            Assert.AreEqual(true, TextInfoModule_PopularTab);

            bool OtherStoriesModule_PopularTab = IsElementDisplayed(OtherStories_Module);
            Assert.AreEqual(true, OtherStoriesModule_PopularTab);
        }

        public void ValidateByTopicMenuINTL()
        {
            Click_Element(ByTopic_Menu);
            verifyIfDisplayed(Car_Accessories_Link);
            verifyIfDisplayed(Human_Made_Link);
            verifyIfDisplayed(Ownership_Link);
            verifyIfDisplayed(Services_Link);
            verifyIfDisplayed(Collaboration_Link);
            verifyIfDisplayed(XC70_Link);
            verifyIfDisplayed(S60CC_Link);

            verifyIfDisplayed(Business_Link);
            verifyIfDisplayed(Working_At_Volvo_Link);
            verifyIfDisplayed(Heritage_Link);
            verifyIfDisplayed(City_Life_Link);

            verifyIfDisplayed(Design_Link);
            verifyIfDisplayed(Environment_Link);
            verifyIfDisplayed(Lifestyle_Link);
            verifyIfDisplayed(Future_Link);

            verifyIfDisplayed(Family_Link);
            verifyIfDisplayed(Safety_Performance_Link);
            verifyIfDisplayed(Technology_Link);
            verifyIfDisplayed(Events_Link);
        }

        public void ValidateByCarMenuINTL()
        {
            Click_Element(ByCar_Menu);
            verifyIfDisplayed(XC90_Link);
            verifyIfDisplayed(XC60_Link);
            verifyIfDisplayed(XC40_Link);
            verifyIfDisplayed(V90_Link);

            verifyIfDisplayed(V60_Link);
            verifyIfDisplayed(V40_Link);
            verifyIfDisplayed(S90_Link);
            verifyIfDisplayed(V60CC_Link);

            verifyIfDisplayed(V40CC_Link);
            verifyIfDisplayed(V90CC_Link);
            verifyIfDisplayed(SUV_Link);
            verifyIfDisplayed(Sedan_Link);

            verifyIfDisplayed(Estate_Link);
            verifyIfDisplayed(CrossCountry_Link);
            verifyIfDisplayed(ConceptCars_Link);
        }

        public void ValidateByTopicMenuUK()
        {
            Click_Element(ByTopic_Menu);
            verifyIfDisplayed(Car_Accessories_Link);
            verifyIfDisplayed(Human_Made_Link);
            verifyIfDisplayed(Ownership_Link);
            verifyIfDisplayed(Services_Link);

            verifyIfDisplayed(Business_Link);
            verifyIfDisplayed(UK_Heritage_Link);
            verifyIfDisplayed(UK_CityLife_Link);
            verifyIfDisplayed(UK_Design_Link);

            verifyIfDisplayed(UK_Environment_Link);
            verifyIfDisplayed(UK_Lifestyle_Link);
            verifyIfDisplayed(UK_Future_Link);
            verifyIfDisplayed(UK_Family_Link);

            verifyIfDisplayed(UK_Safety_Performance_Link);
            verifyIfDisplayed(UK_Technology_Link);
            verifyIfDisplayed(UK_Events_Link);
            verifyIfDisplayed(UK_Collaboration_Link);
        }

        public void ValidateByCarMenuUK()
        {
            Click_Element(ByCar_Menu);
            verifyIfDisplayed(XC90_Link);
            verifyIfDisplayed(XC60_Link);
            verifyIfDisplayed(XC40_Link);
            verifyIfDisplayed(V90_Link);

            verifyIfDisplayed(V60_Link);
            verifyIfDisplayed(V40_Link);
            verifyIfDisplayed(S90_Link);
            verifyIfDisplayed(UK_S60_Link);

            verifyIfDisplayed(UK_V90CC_Link);
            verifyIfDisplayed(UK_V60CC_Link);
            verifyIfDisplayed(UK_V40CC_Link);
            verifyIfDisplayed(SUV);

            verifyIfDisplayed(Estate_Link);
            verifyIfDisplayed(Sedan);
            verifyIfDisplayed(CrossCountry);
        }
        #endregion

        #region ChildSeats
        public void ChildSeatINTL()
        {
            //Verify image is displayed
            Assert.IsTrue(IsElementDisplayed(intl_babySeat));
            Assert.IsTrue(IsElementDisplayed(intl_back2childSeat));
            Assert.IsTrue(IsElementDisplayed(intl_boosterSeat));
            Assert.IsTrue(IsElementDisplayed(intl_boosterSeatcushion));

            //Verify Read more page
            (FindElement(intl_readmoreInfantSeat)).Click();
            Assert.IsTrue(FindElement(infantseatImage).Displayed);
            Assert.IsTrue(FindElement(infantseatHeader).Displayed);
            Assert.IsTrue(FindElement(rearwardFacingseatImage).Displayed);
            Assert.IsTrue(FindElement(rearwardFacingseatHeader).Displayed);
            Assert.IsTrue(FindElement(boosterSeatImage).Displayed);
            Assert.IsTrue(FindElement(boosterSeatHeader).Displayed);
            Assert.IsTrue(FindElement(boosterSeatCushionImage).Displayed);
            Assert.IsTrue(FindElement(boosterSeatCushionHeader).Displayed);
        }

        public void ChildSeatUK()
        {
            //Click_Element("//*[@id='accept']");
            //Verify image is displayed
            Assert.IsTrue(IsElementDisplayed(se_infantSeat));
            Assert.IsTrue(IsElementDisplayed(se_childSeat));
            Assert.IsTrue(IsElementDisplayed(se_highchairbooster));
            Assert.IsTrue(IsElementDisplayed(se_boosterSeatbackrest));

            //Verify Read more page
            FindElement(readmoreboosterSeatbackrest).Click();
            Assert.IsTrue(IsElementDisplayed(boosterSeatImage));
            Assert.IsTrue(IsElementDisplayed(boosterSeatHeader));
            Assert.IsTrue(IsElementDisplayed(boosterSeatCushionImage));
            Assert.IsTrue(IsElementDisplayed(boosterSeatCushionHeader));
        }
        #endregion

        #region LXP
        public void SignInUK()
        {
            Click_Element(icon_lxplogin);
            Click_Element(link_signin);
            EnterText(text_username, text_username_val);
            EnterText(text_password, text_password_val);

            verifyIfDisplayed(link_forgotpassword);
            verifyIfDisplayed(link_whatisvolvoid);
            verifyIfDisplayed(link_getvolvoid);

            Click_Element(btn_login);

            Click_Element(icon_lxplogin);

            Verifytext(LXPMenu_mycars, LXPMenu_mycars_val);
            Verifytext(LXPMenu_preferredretailer, LXPMenu_preferredretailer_val);
            Verifytext(LXPMenu_Newstories, LXPMenu_Newstories_val);
            Verifytext(LXPMenu_Offers, LXPMenu_Offers_val);
            Verifytext(LXPMenu_profilesetting, LXPMenu_profilesetting_val);
            Verifytext(LXPMenu_signout, LXPMenu_signout_val);
        }

        public void AddCarsUK()
        {
            Click_Element(LXPMenu_mycars);
            waitfor(3000);
            //AddCar
            EnterText(txtbx_vincarinput, txtbx_vincarinput_val);
            Click_Element(btn_addcar);
            waitfor(5000);
            verifyattributepresent(addcar_sampleimg, "src");
            verifyIfDisplayed(addcar_modelname);
            verifyIfDisplayed(addcar_productionyr);
            verifyIfDisplayed(addcar_carrelation);
            Verifytext(addcar_vinnum, txtbx_vincarinput_val);
            //confirm
            Click_Element(addcar_confirmbtn);
            waitfor(12000);
            //Verifytext(addsuccessmsg, addsuccessmsgval);
            //verify car added
            verifyattributepresent(addedcar_img, "src");
            verifyIfDisplayed(addedcar_modelname);
            verifyIfDisplayed(addedcar_modelyr);
            verifyIfDisplayed(addedcar_color);
            verifyIfDisplayed(addedcar_relation);
            Verifytext(addedcar_vin, txtbx_vincarinput_val);
            verifyIfDisplayed(addedcar_Licenseplate);
            verifyIfDisplayed(addcar_setasmaincarlink);
            Click_Element(addcar_changecarreltnlink);
            waitfor(3000);
            Click_Element(changecar_reqownerradio);
            Click_Element(changecar_confirmbtn);
            waitfor(10000);
            //setas main
            Click_Element(addcar_setasmaincarlink);
            waitfor(10000);
            string addedcar_maincartxt = SelectText(addedcar_maincar);
            Verifytext(addedcar_maincartxt, addedcar_maincartxt_val);
            //remove added car
            verifyIfDisplayed(addcar_setasmaincarlink);
            Click_Element(addcar_changecarreltnlink);
            Click_Element(changecar_removeradio);
            Click_Element(changecar_confirmbtn);
            waitfor(6000);
            Verifytext(addedcar_removedsuccessmsg, addedcar_removedsuccessmsgval);
        }

        public void ValidatePreferredDealer()
        {
            Click_Element(LXPMenu_preferredretailer);
            if ((driver.FindElement(By.XPath(prefdlr_changelink)).Displayed.ToString()).Equals("True"))
            {
                Click_Element(prefdlr_changelink);
            }
            EnterText(prefdlr_srchtxtbx, prefdlr_zipcode);
            Click_Element(prefdlr_searchicon);
            waitfor(3000);
            string searchresult = SelectText(prefdlr_searchresult);
            Click_Element(prefdlr_searchresult);
            Click_Element(prefdlr_savebtn);
            waitfor(6000);
            string preffereddealer = SelectText(prefdlr_selecteddlr);
            Assert.AreEqual(preffereddealer, searchresult);
        }

        public void validateprofilesetting()
        {
            Click_Element(LXPMenu_profilesetting);
            waitfor(3000);
            Verifytext(PRofsetting_hdr, PRofsetting_hdr_val);
            verifyIfDisplayed(prof_editvolvoid_link);
            verifyIfDisplayed(prof_changepwd_link);
            verifyIfDisplayed(prof_deletevolvoid_link);

            Verifytext(prof_myprofilehdr, prof_myprofilehdr_val);
            verifyIfDisplayed(prof_title_dropdown);
            verifyIfDisplayed(prof_FirstName_textbox);
            verifyIfDisplayed(prof_Lastname_textbox);
            Verifytext(prof_homeaddr_hdr, prof_homeaddr_hdr_val);
            verifyIfDisplayed(prof_Line1_txtbox);
            verifyIfDisplayed(prof_Line2_txtbox);
            verifyIfDisplayed(prof_Line3_txtbox);
            verifyIfDisplayed(prof_Line4_txtbox);
            verifyIfDisplayed(prof_postcode_txtbox);
            verifyIfDisplayed(prof_city_txtbox);
            verifyIfDisplayed(prof_Country_dropdown);
            verifyIfDisplayed(prof_phone_txtbox);
            verifyIfDisplayed(prof_mobile_txtbox);
            verifyIfDisplayed(prof_email_txtbox);
            Verifytext(prof_prefcommuni_hdr, prof_prefcommuni_hdr_val);
            verifyIfDisplayed(prof_email_radio);
            verifyIfDisplayed(prof_phone_radio);
            verifyIfDisplayed(prof_Directmail_radio);
            verifyIfDisplayed(prof_sms_radio);

            //((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0,1000)");
            string PhoneNumber = GenerateNumber();
            EnterText(prof_phone_txtbox, PhoneNumber);
            Click_Element(LXP_profset_savebtn);
            waitfor(3000);
            string pagetitle = PageTitle();
            Assert.AreEqual("Profile and Settings | Volvo Cars UK Ltd", pagetitle);
            string ActualPhoneNumber = GetattributeValue(prof_phone_txtbox, "value");
            Assert.AreEqual(PhoneNumber, ActualPhoneNumber);
        }
        #endregion

        #region CarLocator
        public void Inventory_Validation()
        {
            IWebElement inventorytab = driver.FindElement(By.XPath(inventoryMenu));
            inventorytab.Click();
            System.Threading.Thread.Sleep(7000);
            EnterText(inventorySearchbar, "38701");
            IWebElement searchbutton = driver.FindElement(By.XPath(inventorySearchButton));
            searchbutton.Click();
            System.Threading.Thread.Sleep(10000);
            Assert.AreEqual("Cars at your selected dealer", driver.FindElement(By.XPath(inventorysearchresult)).Text);

        }

        public void InvetorySearchResultsValidation()
        {
            //LocalInventoryValidation
            CarGridValidation();

            //ExtendedInventoryValidation
            Click_Element(extendedInventoryButton);
            CarGridValidation();
        }

        public void CarGridValidation()
        {
            List<IWebElement> carGridView = new List<IWebElement>();

            for (int id = 1; id < 13; id++)
            {
                carGridView.Add(driver.FindElement(By.XPath(".//*[@id='volvo']/div[4]/div/div[4]/div/ul/li[" + id + "]")));
            }
            foreach (IWebElement carView in carGridView)
            {
                Assert.IsTrue(carView.Displayed, "Car Grid view not displayed");
            }
        }

        public void ValidateProductDetailPage()
        {
            Click_Element(selectGridCarLocator);
            System.Threading.Thread.Sleep(2000);
            //click navigate right
            Click_Element(navigateRightArrow);
            FindElement(exteriorImage2);
            Click_Element(navigateRightArrow);
            FindElement(exteriorImage3);
            Click_Element(navigateRightArrow);
            FindElement(exteriorImage4);
            //navigate left
            Click_Element(navigateLeftArrow);
            FindElement(exteriorImage3);
            //ProductOverview
            verifyIfDisplayed(ModelYear);
            verifyIfDisplayed(FuelEconomy);
            verifyIfDisplayed(DriveTrain);
            verifyIfDisplayed(ExteriorColour);
            verifyIfDisplayed(InteriorColour);
            verifyIfDisplayed(VIN);
            //ValidateGetQuote
            Assert.AreEqual("Get a quote", SelectText(getAQuoteText));
            EnterText(FirstNameTextbox, "Test");
            EnterText(LastNameTextbox, "Test");
            EnterText(StreetAddressTextbox, "123");
            EnterText(CityTextbox, "abc");
            SelectDropdownValue(StateDropDown, "FL");
            EnterText(ZipcodeTextbox, "38701");
            EnterText(EmailTextbox, "test@volvocars.com");
            EnterText(PhoneTextbox, "1234567890");
            Click_Element(SendToDealerButton);
            waitfor(5000);

            //string newTabHandle = driver.WindowHandles[1];
            //driver.SwitchTo().Window(newTabHandle);
            verifyIfDisplayed(RequestSentText);
            Click_Element(CloseButton);
            //driver.SwitchTo().Window(driver.WindowHandles[0]);
            verifyIfDisplayed(ConfirmationMessage);
        }
        #endregion

        #region DcomItaly
        public void OrderOnlineNavigation()
        {
            //Clicking Select Your Volvo Link
            Verifytext(selectYourVolvo, selectYourVolvo_txt);
            Click_Element(selectYourVolvo);
            //Validating xc60 content
            Mouseoverelement(heroContentXC60);
            Verifytext(title_XC60, title_XC60_txt);
            Verifytext(xc60OrderOnlineText, orderOnlineBtn_txt);
            FindElement(xc60OrderOnline_btn);
            //validating v90cc content
            Mouseoverelement(heroContentV90CC);
            Verifytext(title_V90CC, title_V90CC_txt);
            Verifytext(v90ccOrderOnlineText, orderOnlineBtn_txt);
            FindElement(v90ccOrderOnline_btn);
            //validating xc90 content
            Mouseoverelement(heroContentXC90);
            Verifytext(title_XC90, title_XC90_txt);
            Verifytext(xc90OrderOnlineText, orderOnlineBtn_txt);
            FindElement(xc90OrderOnline_btn);
        }

        public void CarRentalsPageValidation_XC60()
        {
            //Navigation to CarRentals Page
            Mouseoverelement(heroContentXC60);
            Click_Element(xc60OrderOnline_btn);

            //SubHeader, XC60Page Title and Image Validations
            //SubHeader Validations
            verifyIfDisplayed(DCOMPageReturn_btn);
            verifyattributepresent(DCOMPageSubHeader_1, "href");
            verifyattributepresent(DCOMPageSubHeader_2, "href");
            verifyattributepresent(DCOMPageSubHeader_3, "href");
            verifyattributepresent(DCOMPageSubHeader_4, "href");

            //XC60Page Title and Image Validations
            VerifyImagePresent(xc60RentalPageCarPageImage);
            verifyIfDisplayed(xc60RentalPageCarInformation);
            verifyIfDisplayed(xc60RentalPagePriceInformation);
            verifyIfDisplayed(xc60RentalPageOfferSummary_link);

            //Rental Details Content Validation
            Verifytext(xc60RentalPageRentalDetailsTitle, RentalPageRentalDetailsTitle_txt);
            verifyIfDisplayed(xc60RentalPageRentalDeatilsPrice);
            verifyIfDisplayed(xc60RentalPageRentalDeatilsParagragh);
            verifyIfDisplayed(xc60RentalPageRentalDetailsEdit_btn);

            //Services Included Content Validation
            Verifytext(xc60RentalPageServicesIncludedTitle, RentalPageServicesIncludedTitle_XC60_txt);
            verifyIfDisplayed(xc60RentalPageServicesIncludedTitle_1);
            verifyIfDisplayed(xc60RentalPageServicesIncludedTitle_2);
            verifyIfDisplayed(xc60RentalPageServicesIncludedTitle_3);
            verifyIfDisplayed(xc60RentalPageServicesIncludedTitle_4);
            verifyIfDisplayed(xc60RentalPageServicesIncludedTitle_5);
            VerifyImagePresent(xc60RentalPageServicesIncludedImage_1);
            VerifyImagePresent(xc60RentalPageServicesIncludedImage_2);
            VerifyImagePresent(xc60RentalPageServicesIncludedImage_3);
            VerifyImagePresent(xc60RentalPageServicesIncludedImage_4);
            VerifyImagePresent(xc60RentalPageServicesIncludedImage_5);

            //Equipment Included Content Validation
            Verifytext(xc60RentalPageEquipmentIncludedTitle, RentalPageEquipmentIncludedTitle_txt);
            verifyIfDisplayed(xc60RentalPageEquipmentIncludedTitle_1);
            verifyIfDisplayed(xc60RentalPageEquipmentIncludedTitle_2);
            verifyIfDisplayed(xc60RentalPageEquipmentIncludedTitle_3);
            verifyIfDisplayed(xc60RentalPageEquipmentIncludedTitle_4);
            VerifyImagePresent(xc60RentalPageEquipmentIncludedImage_1);
            VerifyImagePresent(xc60RentalPageEquipmentIncludedImage_2);
            VerifyImagePresent(xc60RentalPageEquipmentIncludedImage_3);
            VerifyImagePresent(xc60RentalPageEquipmentIncludedImage_4);
        }

        public void RentalFormPageValidation_XC60()
        {
            //Navigation to RentalForm page
            Click_Element(xc60RentalPageContinueButton);

            //SubHeader, XC60Page Title and Image Validations
            //SubHeader Validations
            verifyIfDisplayed(DCOMPageReturn_btn);
            verifyattributepresent(DCOMPageSubHeader_1, "href");
            verifyattributepresent(DCOMPageSubHeader_2, "href");
            verifyattributepresent(DCOMPageSubHeader_3, "href");
            verifyattributepresent(DCOMPageSubHeader_4, "href");

            //XC60Page Title and Image Validations
            VerifyImagePresent(xc60FormPageCarImage);
            verifyIfDisplayed(xc60FormPageCarInformation);
            verifyIfDisplayed(xc60FormPagePriceInformation);
            verifyIfDisplayed(xc60FormPageOfferSummary_link);

            //Insert Your Data Form Validation
            verifyIfDisplayed(xc60FormPageInsertYourDataTitle);
            verifyIfDisplayed(xc60FormPageTitle_label);
            verifyIfDisplayed(xc60FormPageName_label);
            verifyIfDisplayed(xc60FormPageSurname_label);
            verifyIfDisplayed(xc60FormPageEmail_label);
            verifyIfDisplayed(xc60FormPageConfirmEmail_label);
            verifyIfDisplayed(xc60FormPageAddress_label);
            verifyIfDisplayed(xc60FormPageLocation_label);
            verifyIfDisplayed(xc60FormPageProvince_label);
            verifyIfDisplayed(xc60FormPagePostalCode_label);
            verifyIfDisplayed(xc60FormPageNation_label);
            verifyIfDisplayed(xc60FormPageFixedTel_label);
            verifyIfDisplayed(xc60FormPageCellPhone_label);

            //StayUptoDate Form Validation
            verifyIfDisplayed(xc60FormPageStayUptoDateTitle);
            verifyIfDisplayed(xc60FormPageStayUptoDateEmail_label);
            verifyIfDisplayed(xc60FormPageStayUptoDateMail_label);
            verifyIfDisplayed(xc60FormPageStayUptoDatePhone_label);
            verifyIfDisplayed(xc60FormPageStayUptoDateSMS_label);
            verifyIfDisplayed(xc60FormPageStayUptoDateEmail_radiobtn_Y);
            verifyIfDisplayed(xc60FormPageStayUptoDateMail_radiobtn_Y);
            verifyIfDisplayed(xc60FormPageStayUptoDatePhone_radiobtn_Y);
            verifyIfDisplayed(xc60FormPageStayUptoDateSMS_radiobtn_Y);
            verifyIfDisplayed(xc60FormPageStayUptoDateEmail_radiobtn_N);
            verifyIfDisplayed(xc60FormPageStayUptoDateMail_radiobtn_N);
            verifyIfDisplayed(xc60FormPageStayUptoDatePhone_radiobtn_N);
            verifyIfDisplayed(xc60FormPageStayUptoDateSMS_radiobtn_N);

            //DealerValidation_XC60
            verifyIfDisplayed(xc60FormPageSelectYourDealerTitle);
            verifyIfDisplayed(xc60FormPageDealer_Table);
            verifyIfDisplayed(xc60FormPageDealer_Map);
            verifyIfDisplayed(xc60FormPageDealer_txtbox);
            verifyIfDisplayed(xc60FormPageDealer_OK_btn);
        }

        public void SubmitOrderPageValidation_XC60()
        {
            //FormSubmission and Navigation to Submit order page
            SelectDropdownValue(xc60FormPageTitle_dropdown, "Signor");
            EnterText(xc60FormPageName_txtbox, "Test");
            EnterText(xc60FormPageSurname_txtbox, "Test");
            EnterText(xc60FormPageEmail_txtbox, "test@volvocars.com");
            EnterText(xc60FormPageConfirmEmail_txtbox, "test@volvocars.com");
            EnterText(xc60FormPageAddress_txtbox, "123");
            EnterText(xc60FormPageLocation_txtbox, "abc");
            EnterText(xc60FormPageProvince_txtbox, "xyz");
            EnterText(xc60FormPagePostalCode_txtbox, "12345");
            SelectDropdownValue(xc60FormPageNation_dropdown, "Italia");
            EnterText(xc60FormPageFixedTel_txtbox, "1234567890");
            EnterText(xc60FormPageCellPhone_txtbox, "2345678901");
            Click_Element(xc60FormPageStayUptoDateEmail_radiobtn_Y);
            Click_Element(xc60FormPageStayUptoDateMail_radiobtn_N);
            Click_Element(xc60FormPageStayUptoDatePhone_radiobtn_N);
            Click_Element(xc60FormPageStayUptoDateSMS_radiobtn_N);
            Click_Element(xc60FormPageDealerName_dealer1);
            Click_Element(xc60FormPageContinueCTA);

            //SubHeader, XC60Page Title and Image Validations
            //SubHeader Validations
            verifyIfDisplayed(DCOMPageReturn_btn);
            verifyattributepresent(DCOMPageSubHeader_1, "href");
            verifyattributepresent(DCOMPageSubHeader_2, "href");
            verifyattributepresent(DCOMPageSubHeader_3, "href");
            verifyattributepresent(DCOMPageSubHeader_4, "href");

            //XC60Page Title and Image Validations
            VerifyImagePresent(xc60SubmitOrderPageCarImage);
            verifyIfDisplayed(xc60SubmitOrderPageCarInformation);
            verifyIfDisplayed(xc60SubmitOrderPagePriceInformation);
            verifyIfDisplayed(xc60SubmitOrderPageOfferSummary_link);

            //SubmitOrder Page Validation
            verifyIfDisplayed(xc60SubmitOrderPageYourDataInformation);
            verifyIfDisplayed(xc60SubmitOrderPageYourDealerInformation);
            verifyIfDisplayed(xc60SubmitOrderPageRentalInformation);

            //Terms&Conditions and Cartcha Validations
            IWebElement xc60SubmitOrderPageTermsAndConditionsLink = driver.FindElement(By.CssSelector("[class='ctaSecondaryInline F493A58FFFE04B58BC3A8C1DCD5BF0C1_open']"));
            xc60SubmitOrderPageTermsAndConditionsLink.Click();
            //".//*[@class='ctaSecondaryInline F493A58FFFE04B58BC3A8C1DCD5BF0C1_open']"
            ScrollIntoView(xc60SubmitOrderPageAcceptCTA);
            Click_Element(xc60SubmitOrderPageAcceptCTA);
            Click_Element(xc60SubmitOrderPageTermsAndConditionsCTA);
            Click_Element(xc60SubmitOrderPageCaptchaCheckbox);
        }
        #endregion

        #region DcomAccessories
        public void uk_validateLandingpage()
        {
            Click_Element(uk_continue_button);
            //verifyIfDisplayed(uk_backgroundimage);
            //Verifytext(uk_txt_volvoacc, uk_Accessories_text_volvoaccheader);
            //verifyIfDisplayed(uk_txt_volvoBodytext);
            Verifytext(uk_txt_autoaccesories, uk_Accessories_text_Autoaccessories);
            Verifytext(uk_txt_lifestylecollect, uk_Accessories_text_Lifestylecollect);

        }
        public void uk_validateAutoAccessoriesPage()
        {
            //Mouseover on Autoaccesories
            Mouseoverelement(uk_txt_autoaccesories);
            verifyIfDisplayed(uk_img_autoaccesoriesimg);
            verifyIfDisplayed(uk_txt_autoaccesoriesbody);
            verifyIfDisplayed(uk_btn_autoacc_collectionbutton);
            //click on autoaccessories button and verify page navigation
            Click_Element(uk_btn_autoacc_collectionbutton);
            waitfor(10000);
            Verifytext(uk_txt_autoaccheader, uk_Accessories_text_autoaccpage_header);
            //navigate back to landing page
            driver.Navigate().Back();
            waitfor(5000);
        }
        public void uk_validateLifestylecollectionPage()
        {
            //Mouseover on Lifestyle
            Mouseoverelement(uk_txt_lifestylecollect);
            verifyIfDisplayed(uk_img_lifestyleimg);
            verifyIfDisplayed(uk_txt_lifestylebody);
            verifyIfDisplayed(uk_btn_lifestyle_collectionbutton);
            //click on lifestylecollecte button and verify page navigation
            Click_Element(uk_btn_lifestyle_collectionbutton);
            waitfor(10000);
            Verifytext(uk_txt_lifestylepgheader, uk_Accessories_text_lifestylecolpage_header);
        }

        public void uk_validateNavigationbar()
        {
            //verify navigation bar in home page
            verifyIfDisplayed(uk_lbl_navigation_accessories);
            verifyIfDisplayed(uk_lbl_navigation_Categories);
            verifyIfDisplayed(uk_lbl_navigation_select_dealer);
            verifyIfDisplayed(uk_lbl_navigation_select_model);
            verifyIfDisplayed(uk_txt_navigation_Search);
        }

        public void uk_DealerLocator()
        {
            Click_Element(uk_lbl_navigation_select_dealer);
            verifyIfDisplayed(uk_selectdealer_title);
            verifyIfDisplayed(uk_searchdealer_textbox);
            verifyIfDisplayed(uk_dealer_accordiantable);
            verifyIfDisplayed(uk_dealermap);
            Click_Element(uk_dealer_selectbutton);
            waitfor(2000);
        }

        public void uk_CarSelector()
        {
            Click_Element(uk_lbl_navigation_select_model);
            waitfor(2000);

            verifyIfDisplayed(uk_carselector_saloon);
            verifyIfDisplayed(uk_carselector_estate);
            verifyIfDisplayed(uk_carselector_suv);
            Click_Element(uk_carselector_saloon_select);

            verifyIfDisplayed(uk_carselector_s60);
            verifyIfDisplayed(uk_carselector_s60cc);
            Click_Element(uk_carselector_s60_ModelYear);
            Click_Element(uk_carselector_s60_ModelYear_DD);
            Click_Element(uk_carselector_s60_selectbutton);
            waitfor(1000);
        }

        public void uk_validatesearchresultpage()
        {
            verifyIfDisplayed(uk_searchResults_sidebar);
            verifyIfDisplayed(uk_searchresults_sidebar_accessories);
            verifyIfDisplayed(uk_searchresults_sidebar_lifestyle);
            verifyIfDisplayed(uk_searchresults_list);
            List<IWebElement> SearchResultItems = new List<IWebElement>();
            for (int i = 1; i <= 10; i++)
            {
                SearchResultItems.Add(driver.FindElement(By.XPath("//*[@id='acc-pagination-scroll']/div[2]/div[" + i + "]/div[1]")));
            }
        }

        public void uk_validatproductdetailspage()
        {
            Click_Element(uk_searchresults_viewitem);
            waitfor(5000);
            verifyIfDisplayed(uk_pdp_backlink);
            verifyIfDisplayed(uk_pdp_img);
            verifyIfDisplayed(uk_pdp_productdetailSection);
            verifyIfDisplayed(uk_pdp_producttitle);
            verifyIfDisplayed(uk_pdp_pricedetails);
            verifyIfDisplayed(uk_pdp_quantity_dropdown);
            verifyIfDisplayed(uk_pdp_productdescription);
            verifyIfDisplayed(uk_pdp_productspec);
            //Click_Element(uk_pdp_pickupbutton);
            Click_Element(uk_pdp_Addtobasketbutton);
        }

        public void uk_checkoutpage()
        {
            Click_Element(uk_basketicon);
            waitfor(2000);

            verifyIfDisplayed(uk_basket_continueshoppinglink);
            Verifytext(uk_basket_title, "Shopping Basket (d)");
            verifyIfDisplayed(uk_basket_item);
            verifyIfDisplayed(uk_ordersummary);
            Click_Element(uk_checkout_button);

            verifyIfDisplayed(uk_backtobasket_link);
            Verifytext(uk_checkout_title, "Checkout");
            verifyIfDisplayed(uk_checkout_contactdetails_title);
            verifyIfDisplayed(uk_checkout_namelabel);
            verifyIfDisplayed(uk_checkout_emaillabel);
            verifyIfDisplayed(uk_checkout_phonenumberlabel);
            verifyIfDisplayed(uk_checkout_preferredcontact_label);
            verifyIfDisplayed(uk_checkout_regnumber_label);
            verifyIfDisplayed(uk_checkout_firstname);
            verifyIfDisplayed(uk_checkout_lastname);
            verifyIfDisplayed(uk_checkout_email);
            verifyIfDisplayed(uk_checkout_confirmemail);
            verifyIfDisplayed(uk_checkout_phonenumber);
            verifyIfDisplayed(uk_checkout_email_button);
            verifyIfDisplayed(uk_checkout_phone_button);
            verifyIfDisplayed(uk_checkout_regnumber);
            verifyIfDisplayed(uk_checkout_captcha);
            verifyIfDisplayed(uk_checkout_item);
        }
        #endregion

        #region DcomRegularSales
        public void CareByVolvoHomePageValidation()
        {
            verifyIfDisplayed(careByVolvoHomePageHeroContent);
            Verifytext(careByVolvoHomePageCareByVolvoTitle, careByVolvoHomePageCareByVolvoTitle_txt);

            verifyIfDisplayed(careByVolvoHomePageIndicationBar);
            verifyIfDisplayed(careByVolvoHomePageHeadertextsection1);
            verifyIfDisplayed(careByVolvoHomePageDealersection);
            verifyIfDisplayed(careByVolvoHomePageHeadertextsection2);
            verifyIfDisplayed(careByVolvoHomePageExfeature);
            verifyIfDisplayed(careByVolvoHomePageJourneyContainer);
            verifyIfDisplayed(careByVolvoHomePageCallChatSection);
        }

        public void DealerSelectionValidation()
        {
            //Validating dealer content in HomePage
            Verifytext(careByVolvoHomePageDealerTitle, careByVolvoHomePageDealerTitle_txt);
            verifyIfDisplayed(careByVolvoHomePageDealerMarker);
            verifyIfDisplayed(careByVolvoHomePageDealerNameContainer);
            verifyIfDisplayed(careByVolvoHomePageChooseDealer_btn);
            
            //Clicking Choose Dealer Button
            Click_Element(careByVolvoHomePageChooseDealer_btn);

            //Validating DealerOverlay Content_Close Icon
            Verifytext(careByVolvoHomePageDealerOverLayTitle, careByVolvoHomePageDealerOverLayTitle_txt);
            verifyIfDisplayed(careByVolvoHomePageDealerOverLayCloseIcon_top);
            verifyIfDisplayed(careByVolvoHomePageDealerOverLay_txtbox);
            verifyIfDisplayed(careByVolvoHomePageDealerOverLaySearch_btn);
            verifyIfDisplayed(careByVolvoHomePageDealerOverLayDealerTable);
            verifyIfDisplayed(careByVolvoHomePageDealerOverLayDealer1);
            verifyIfDisplayed(careByVolvoHomePageDealerOverLayCloseButton);
            verifyIfDisplayed(careByVolvoHomePageDealerOverLayAcceptButton);

            //Selecting Dealer 
            Click_Element(careByVolvoHomePageDealerOverLayDealer1);
            waitfor(2000);
            string careByVolvoHomePageDealerOverLayDealer1Nametxt = driver.FindElement(By.XPath(careByVolvoHomePageDealerOverLayDealer1Name)).Text;
            Click_Element(careByVolvoHomePageDealerOverLayAcceptButton);
            
            //Validating selected dealer
            string careByVolvoHomePageSelectedDealerNametxt = driver.FindElement(By.XPath(careByVolvoHomePageSelectedDealerName)).Text;
            Assert.AreEqual(careByVolvoHomePageDealerOverLayDealer1Nametxt, careByVolvoHomePageSelectedDealerNametxt);
        }

        public void BuildPageValidation()
        {
            Click_Element(careByVolvoHomePageDealerContinueButton);
            waitfor(3000);

            ScrollIntoView(careByvolvoBuildXC40);
            Click_Element(careByvolvoBuildXC40);
            waitfor(5000);

            Click_Element(careByVolvoBuildOkButton);
            verifyIfDisplayed(careByVolvoBuildTrim1);
            verifyIfDisplayed(careByVolvoBuildTrim2);

            Click_Element(careByVolvoBuildEngineTab);
            waitfor(2000);
            verifyIfDisplayed(careByVolvoBuildEngine1);
            verifyIfDisplayed(careByVolvoBuildEngine2);

            Click_Element(careByVolvoBuildDesignTab);
            waitfor(2000);
            verifyIfDisplayed(careByVolvoBuildDesignVisualizationsection);
            verifyIfDisplayed(careByVolvoBuildDesignSelectionsection);
            verifyIfDisplayed(careByVolvoBuildDesignSidebarsection);

            Click_Element(careByVolvoBuildPacksTab);
            waitfor(2000);
            verifyIfDisplayed(careByVolvoBuildPack1);
            verifyIfDisplayed(careByVolvoBuildPack2);
            verifyIfDisplayed(careByVolvoBuildPack3);
            verifyIfDisplayed(careByvolvoBuildPack4);
            verifyIfDisplayed(careByvolvoBuildPack5);

            Click_Element(careByVolvoBuildSummaryTab);
            waitfor(2000);
            verifyIfDisplayed(careByVolvoBuildSummaryVisualization);
            verifyIfDisplayed(careByVolvoBuildSummaryIntro);
            verifyIfDisplayed(careByVolvoBuildSummaryDetails);
            verifyIfDisplayed(careByVolvoBuildSummaryFeatures);

            Click_Element(careByVolvoBuildSummaryContinueButton);
            waitfor(5000);
        }

        public void FinancialoptionsValidation()
        {
            verifyIfDisplayed(careByVolvoFinancialOptionsBackLink);
            verifyIfDisplayed(careByVolvoFinancialOptionsIndicationBar);
            verifyIfDisplayed(careByVolvoFinancialOptionsCarImage);
            verifyIfDisplayed(careByVolvoFinancialOptionsCarDetails);

            verifyIfDisplayed(careByVolvoFinancialOptionsAgreementSection);
            verifyIfDisplayed(careByVolvoFinancialOptionsAppointmentInformation);
            verifyIfDisplayed(careByVolvoFinancialOptionsPricingDetails);
            Click_Element(careByVolvoFinancialOptionsSeeMoreLink);

            verifyIfDisplayed(careByVolvoFinancialOptionsMorePricingDetails);
            verifyIfDisplayed(careByVolvoFinancialOptionsSeeLessLink);

            verifyIfDisplayed(careByVolvoFinancialOptionsLoginHeader);
            verifyIfDisplayed(careByVolvoFinancialOptionsBankIDButton);
            verifyIfDisplayed(careByVolvoFinancialOptionsMobileBankIDButton);

            verifyIfDisplayed(careByVolvoFinancialOptionsJourneyContainer);
            verifyIfDisplayed(careByVolvoFinancialOptionsCallChatSection);

            Click_Element(careByVolvoFinancialOptionsBankIDButton);
            waitfor(2000);
        }
        #endregion

        public void OrderPreviewPageS90()
        {
            verifyIfDisplayed(s90OrderPreviewHeader);
            verifyIfDisplayed(s90OrderPreviewTitle);
            verifyIfDisplayed(s90OrderPreviewImage);
            verifyIfDisplayed(s90OrderPreviewPriceDetails);

            EnterText(s90firstNameTextBox, FormInput_name);
            EnterText(s90lastNameTextBox, FormInput_name);
            EnterText(s90streetAddressTextBox, FormInput_name);
            EnterText(s90cityTextBox, FormInput_name);
            EnterText(s90stateTextBox, FormInput_state);
            EnterText(s90zipCodeTextBox, FormInput_zipcode);
            System.Threading.Thread.Sleep(2000);
            EnterText(s90emailAddressTextBox, FormInput_email);
            EnterText(s90emailConfirmationTextBox, FormInput_email);
            EnterText(s90phoneTextBox, FormInput_phone);

            verifyIfDisplayed(s90OrderPreviewPreferredDealer);
            verifyIfDisplayed(s90OrderPreviewDealerMap);
            verifyIfDisplayed(s90OrderPreviewSelectedDealer);
            verifyIfDisplayed(s90OrderPreviewChangeDealerLink);

            verifyIfDisplayed(s90OrderPreviewDelivery);
            verifyIfDisplayed(s90OrderPreviewBacktobuild);
            Click_Element(s90ContinueToOrderSummaryButton);
        }

       public void OrderSummaryPageS90()
        {
            verifyIfDisplayed(s90OrdersummaryHeader);
            verifyIfDisplayed(s90OrdersummaryTitle);
            verifyIfDisplayed(s90OrdersummaryImage);
            verifyIfDisplayed(s90OrdersummaryPriceDeatils);

            verifyIfDisplayed(s90OrdersummaryPersonalDetails);
            verifyIfDisplayed(s90OrdersummaryDealerDetails);
            verifyIfDisplayed(s90OrdersummaryDelivery);
            verifyIfDisplayed(s90OrdersummaryDepositAndTotalPrice);
            verifyIfDisplayed(s90OrderSummaryBackToDetails);

            Click_Element(s90OrdersummaryPaymentObligation);
            Click_Element(s90OrdersummaryTermsAndConditions);
            Click_Element(s90OrderSummaryContinueToPaymentButton);
        }

        public void PaymentPage()
        {
            verifyIfDisplayed(PaymentPageTitle);
            verifyIfDisplayed(PaymentPageCardTitle);
            verifyIfDisplayed(PaymentPageForm);
            verifyIfDisplayed(PaymentPageConfirmAmt);
            verifyIfDisplayed(PaymentPagePaymentButton);
            verifyIfDisplayed(PaymentPageCancelPaymentLink);
        }
    }
}
